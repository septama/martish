class NotificationUser{
  String _username;
  int _userId;
  String _profilePicture;
  String _timeText;
  String _time;
  String _desc;
   NotificationUser.map(dynamic obj){
    this._userId = obj['user_id'];
    this._username = obj['username'];
    this._profilePicture = obj['profile_picture'];
    this._desc = obj['text'];
    this._time = obj['read_at'];
    this._timeText = obj['time_text'];
  }
  NotificationUser.fromMap(dynamic obj)
  :
    this._userId = obj['user_id'],
    this._username = obj['username'],
    this._profilePicture = obj['profile_picture'],
    this._desc = obj['text'],
    this._time = obj['read_at'],
    this._timeText = obj['time_text'];
  
  int get userId => this._userId;
  String get username => this._username;
  String get profilePicture => this._profilePicture;
  String get timeText => this._timeText;
  String get time => this._time;
  String get desc => this._desc;

}
class NotificationData{
  List<dynamic> _data = new List();
  String _message;
  String _nextPageUrl;
  bool _status;

  String get nextPageUrl => this._nextPageUrl;
  String get message => this._message;
  List get data => this._data;
  bool get status => this._status;
  NotificationData.fromMap(Map<String, dynamic> map)
      : _data = map['result']['data'],
        _message = map['msg'],
        _nextPageUrl = map['result']['next_page_url'],
        _status = map['success'];
  NotificationData.map(dynamic map){
       _data = map['result']['data'];
        _message = map['msg'];
        _nextPageUrl = map['result']['next_page_url'];
        _status = map['success'];
  }
}
abstract class NotificationRepository{
  Future<NotificationData> getNotifications(String token,String queryParam);
}