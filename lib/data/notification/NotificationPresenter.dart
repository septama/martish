import 'package:martish/data/fetchDataException.dart';
import 'package:martish/data/notification/NotificationData.dart';
import 'package:martish/data/user/UserData.dart';
import 'package:martish/data/notification/di.dart';

abstract class ListNotificationContract{
  void onGetNotificationSuccess(NotificationData notificationData);
  void onGetNotificationFailed(FetchDataException error);
  void onRefreshUserSuccess(User user);
  void onRefreshUserError(FetchDataException error);
}
class ListNotificationPresenter{
  ListNotificationContract _view;
  NotificationRepository _notificationRepository;
  UserRepository _userRepository;
  ListNotificationPresenter(this._view){
    this._notificationRepository = new Injector().notificationRepository;
    this._userRepository = new Injector().userRepository;
  }
  void refreshUser(){
    _userRepository.refreshUser().
    then((data)=> _view.onRefreshUserSuccess(data)).
    catchError((e)=>_view.onRefreshUserError(new FetchDataException(e.toString())));
  }
  void loadNotification(String token,String queryparam){
    _notificationRepository.getNotifications(token, queryparam)
    .then((onValue)=>_view.onGetNotificationSuccess(onValue))
    .catchError((onError)=>_view.onGetNotificationFailed(new FetchDataException(onError.toString())));
  }
}