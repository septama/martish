import 'dart:convert';
import 'dart:io';

import 'package:martish/data/config.dart';
import 'package:martish/data/fetchDataException.dart';
import 'package:martish/data/notification/NotificationData.dart';
import 'package:http/http.dart' as http;
class NotificationServiceRepository implements NotificationRepository{
  static Config config  = new Config();
  static String base_url =  config.host;
  static String prefix = config.prefix;
  @override
  Future<NotificationData> getNotifications(String token, String queryparam) async{
    print("Fetch Notification");
    // TODO: implement fetchPosts
    var url = "";
    if(queryparam !=null){
      url = base_url+prefix+'/notification'+queryparam;
    }
    else{
      url = base_url+prefix+'/notification';
    }
    http.Response response = await http.get(
      url,
      headers: {
        HttpHeaders.authorizationHeader : "Bearer "+token,
        HttpHeaders.contentTypeHeader : 'application/x-www-form-urlencoded',
      },
    );
    final statusCode = response.statusCode;
    final Map responseBody = json.decode(response.body);
    print(responseBody);
    final NotificationData _dataNotification = new NotificationData.map(responseBody);
    if(statusCode < 200 || statusCode > 400){
      throw new FetchDataException(
          "An error ocurred : [Status Code : $statusCode]");
    }
    return _dataNotification;
  }
}