import 'package:martish/data/fetchDataException.dart';
import 'package:martish/data/profile/ProfileData.dart';
import 'package:martish/data/profile/di.dart';
import 'package:martish/data/user/UserData.dart';
import 'package:martish/data/activity/ActivityData.dart';
import 'package:martish/data/post/PostData.dart';

abstract class ProfileScreenContract{
  void onRefreshUserSuccess(User user);
  void onRefreshUserError(FetchDataException error);
  void onLoadPostSuccess(DataPost dataPost);
  void onLoadPostError(FetchDataException error);
  void onLoadProfileSuccess(UserProfileData userProfileData);
  void onLoadProfileError(FetchDataException error);
}
class ProfileScreenPresenter{
  ProfileScreenContract _view;
  UserRepository _userRepository;
  UserProfileRepository _userProfileRepository;
  PostRepository _postRepository;
  ActivityUserRepository _activityUserRepository;
  ProfileScreenPresenter(this._view){
    _userRepository = new Injector().userRepository;
    _userProfileRepository = new Injector().userProfileRepository;
    _postRepository = new Injector().postRepository;
    _activityUserRepository = new Injector().activityUserRepository;
  }
  void refreshUser(){
    _userRepository.refreshUser().
    then((data)=> _view.onRefreshUserSuccess(data)).
    catchError((e)=>_view.onRefreshUserError(new FetchDataException(e.toString())));
  } 
  void loadProfile(String token){
    _userProfileRepository.getMyProfile(token).
    then((onValue)=>_view.onLoadProfileSuccess(onValue))
    .catchError((onError)=>_view.onLoadProfileError((new FetchDataException(onError.toString()))));
  }
  void loadMyPost(String token, String queryParam){
    _postRepository.fetchPostsByUser(token, queryParam)
    .then((onValue)=>_view.onLoadPostSuccess(onValue))
    .catchError((onError) => _view.onLoadPostError((FetchDataException(onError.toString()))));
  }
}