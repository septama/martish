import 'package:martish/data/activity/ActivityData.dart';
import 'package:martish/data/activity/ActivityService.dart';
import 'package:martish/data/post/PostData.dart';
import 'package:martish/data/post/PostService.dart';
import 'package:martish/data/profile/ProfileData.dart';
import 'package:martish/data/profile/ProfileService.dart';
import 'package:martish/data/user/UserData.dart';
import 'package:martish/data/user/UserService.dart';

class Injector{
   static final Injector _singleton = new Injector._internal();
  
    factory Injector() {
      return _singleton;
    }
  
    Injector._internal();

  UserProfileRepository get userProfileRepository => new UserProfileServiceRepository();
  ActivityUserRepository get activityUserRepository => new ActivityUserServiceRepository();
  PostRepository get postRepository => new PostServiceRepostory();
  UserRepository get userRepository => new UserServiceRepository();
}