import 'dart:io';

import 'package:flutter/rendering.dart';

class UserProfile{
  String _profilePicture;
  String _bannerPicture;
  String _bio;
  String _website;
  String _birthday;
  String _fullName;
  String _username;
  String _gender;
  int _countPost;
  int _countFollower;
  int _countFollowing;
  int _id;
  int _userId;
  String _privateAccount;
  String _userRelationships;
  int get id => _id;
  int get userId => _userId;
  String get profilePicture => _profilePicture;
  String get bannerPicture => _bannerPicture;
  String get bio => _bio;
  String get website => _website;
  String get birthday => _birthday;
  String get fullName => _fullName;
  String get gender => _gender;
  String get username => _username;
  int get countFollower => _countFollower;
  int get countFollowing => _countFollowing;
  int get countPost => _countPost;
  String get privateAccount =>_privateAccount;
  String get userRelationship =>_userRelationships;
  UserProfile.map(dynamic obj){
    _id = obj['id'];
    _userId = obj['user_id'];
    _profilePicture = obj['profile_picture'];
    _bannerPicture = obj['banner_picture'];
    _bio = obj['bio'];
    _website = obj['website'];
    _birthday =obj['birthday'];
    _fullName = obj['full_name'];
    _gender = obj['gender'];
    _username = obj['username'];
    _privateAccount = obj['private_account'];
    _userRelationships = obj['user_relationships'];
    _countFollower = obj['count']['count_follower'];
    _countFollowing = obj['count']['count_following'];
    _countPost = obj['count']['count_post'];
  }
  UserProfile.fromMap(Map<String, dynamic> obj)
  :
    _id = obj['id'],
    _userId = obj['user_id'],
    _profilePicture = obj['profile_picture'],
    _bannerPicture = obj['banner_picture'],
    _bio = obj['bio'],
    _website = obj['website'],
    _birthday =obj['birthday'],
    _fullName = obj['full_name'],
    _privateAccount = obj['private_account'],
    _gender = obj['gender'],
    _countFollower = obj['count']['count_follower'],
    _countFollowing = obj['count']['count_following'],
    _countPost = obj['count']['count_post'],
    _userRelationships = obj['user_relationships'],
    _username = obj['username'];
}
class UserProfileData{
  UserProfile _userProfile;
  String _message;
  bool _success;
  dynamic _updateProfile;
  dynamic _updateProfilePic;
  dynamic _updateBannerPic;
  UserProfile get userProfile => _userProfile;
  String get message => _message;
  bool get success => _success;
  dynamic get updateProfilePic => _updateProfilePic;
  dynamic get updateProfile => _updateProfile;
  dynamic get updateBannerPic => _updateBannerPic;
  UserProfileData.map(dynamic obj){
    _userProfile = new UserProfile.map(obj['profile']);
    _message = obj['msg'];
    _success = obj['success'];
    _updateProfile = obj['update_profile'];
    _updateBannerPic = obj['update_banner_picture'];
    _updateProfilePic = obj['update_profile_picture'];
  }
}
abstract class UserProfileRepository{
  Future<UserProfileData> getMyProfile(String token);
  Future<UserProfileData> getUserProfile(String token, int userId);
  Future<dynamic>changeProfilePicture(String token, File image);
  Future<dynamic>changeBannerPicture(String token, File image);
  Future<UserProfileData>updateProfile(String token,dynamic data);
}