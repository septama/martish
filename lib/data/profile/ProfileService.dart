import 'dart:convert';
import 'dart:io';

import 'package:martish/data/fetchDataException.dart';
import 'package:martish/data/profile/ProfileData.dart';
import 'package:martish/data/config.dart';
import 'package:http/http.dart' as http;
class UserProfileServiceRepository implements UserProfileRepository{
  static Config config  = new Config();
  static String base_url =  config.host;
  static String prefix = config.prefix;
  
  @override
  Future<UserProfileData> getMyProfile(String token) async{
    String url = base_url+prefix+'/profile';
    http.Response response = await http.get(
      url,
      headers: {
        HttpHeaders.authorizationHeader : 'Bearer '+token,
        HttpHeaders.contentTypeHeader : 'application/x-www-form-urlencoded',
      }
    );
    // TODO: implement getMyProfile
    final statusCode = response.statusCode;
    final Map responseBody = json.decode(response.body);
    print(responseBody);
    final UserProfileData _userProfileData = new UserProfileData.map(responseBody);
     if(statusCode < 200 || statusCode > 400){
      throw new FetchDataException(
          "An error ocurred : [Status Code : $statusCode]");
    }
    return _userProfileData;
  }

  @override
  Future changeBannerPicture(String token, File image) {
    // TODO: implement changeBannerPicture
    return null;
  }

  @override
  Future changeProfilePicture(String token, File image) {
    // TODO: implement changeProfilePicture
    return null;
  }

  @override
  Future<UserProfileData> updateProfile(String token, data) {
    // TODO: implement updateProfile
    return null;
  }

  @override
  Future<UserProfileData> getUserProfile(String token, int userId) async {
    // TODO: implement getUserProfile
    String url = base_url+prefix+'/profile/user';
    if(userId!=null){
      url = url + '?user_id=' + userId.toString();
    }
    http.Response response = await http.get(
      url,
      headers: {
        HttpHeaders.authorizationHeader : 'Bearer '+token,
        HttpHeaders.contentTypeHeader : 'application/x-www-form-urlencoded',
      }
    );
    // TODO: implement getMyProfile
    final statusCode = response.statusCode;
    final Map responseBody = json.decode(response.body);
    print(responseBody);
    final UserProfileData _userProfileData = new UserProfileData.map(responseBody);
     if(statusCode < 200 || statusCode > 400){
      throw new FetchDataException(
          "An error ocurred : [Status Code : $statusCode]");
    }
    return _userProfileData;
  }

}