import 'package:martish/data/like/LikeData.dart';
import 'package:martish/data/like/LikeService.dart';
import 'package:martish/data/user/UserData.dart';
import 'package:martish/data/user/UserService.dart';


class Injector {
  static final Injector _singleton = new Injector._internal();
  
    factory Injector() {
      return _singleton;
    }
  
    Injector._internal();

  LikerRepository get likerRepository => new LikeServiceRepository();
  UserRepository get userRepository => new UserServiceRepository();
}
