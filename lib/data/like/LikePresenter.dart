import 'package:martish/data/fetchDataException.dart';
import 'package:martish/data/like/di.dart';

import 'LikeData.dart';

abstract class LikerContract{
  void onLoadLikerSuccess(List<Liker> liker,int currentPage,String nextPageUrl);
  void onLoadLikerError(FetchDataException error);
}
class LikerPresenter{
  LikerContract _view;
  LikerRepository _likerRepository;
  LikerPresenter(this._view){
    _likerRepository = new Injector().likerRepository;
  }
  void loadLiker(String token,String queryparam){
    _likerRepository.fetchLikers(token, queryparam).
    then((onValue)=>
    _view.onLoadLikerSuccess(
      onValue.likerList.map((f)=>Liker.fromMap(f)).toList(),
      onValue.currentPage,
      onValue.nextPageUrl
    )).catchError((e)=>_view.onLoadLikerError(new FetchDataException(e.toString())));
  }
}

