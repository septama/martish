import 'dart:convert';
import 'dart:io';
import 'package:martish/data/like/LikeData.dart';

import 'package:martish/data/config.dart';
import 'package:martish/data/fetchDataException.dart';
import 'package:http/http.dart' as http;


class LikeServiceRepository implements LikerRepository{
  static Config config  = new Config();
  static String base_url =  config.host;
  static String prefix = config.prefix;
  @override
  Future<Liker> fetchLiker() {
    // TODO: implement fetchLiker
    return null;
  }

  @override
  Future<LikerData> fetchLikers(String token, String queryParam) async{
    // TODO: implement fetchLikers
    print("Fetch Liker");
    var url = "";
    print(queryParam);
    if(queryParam !=null){
      url = base_url+prefix+'/post/like'+queryParam;
    }
    else{
      url = base_url+prefix+'/post/like';
    }
    http.Response response = await http.get(
      url,
       headers: {
        HttpHeaders.authorizationHeader : "Bearer "+token,
        HttpHeaders.contentTypeHeader : 'application/x-www-form-urlencoded',
      },
    );
    final statusCode = response.statusCode;
    final Map responseBody = json.decode(response.body);
    print(responseBody);
    final LikerData _likerData = new LikerData.map(responseBody);
    if(statusCode < 200 || statusCode > 400){
      throw new FetchDataException(
          "An error ocurred : [Status Code : $statusCode]");
    }
    print("Returning comment data");
    return _likerData;
  }
  
}