class Liker{
  int _postId;
  int _id;
  int _userId;
  String _profilePicture;
  String _username;
  String _createdAt;
  String _userRelationshipStatus;
  Liker(this._postId,this._id,this._userId,this._createdAt,this._profilePicture,this._username,this._userRelationshipStatus);
  Liker.map(dynamic obj){
    this._postId = obj['post_id'];
    this._id = obj['id'];
    this._username = obj['username'];
    this._userId = obj['user_id'];
    this._profilePicture = obj['profile_picture'];
    this._createdAt = obj['created_at'];
    this._userRelationshipStatus = obj['user_relationship_status'];
  }
  Liker.fromMap(Map<String,dynamic>obj)
  : this._postId = obj['post_id'],
    this._id = obj['id'],
    this._userId = obj['user_id'],
    this._username = obj['username'],
    this._profilePicture = obj['profile_picture'],
    this._createdAt = obj['created_at'],
    this._userRelationshipStatus = obj['user_relationship_status'];
  int get id => this._id;
  int get userId => this._userId;
  String get profilePicture => this._profilePicture;
  String get createdAt => this._createdAt;
  String get username=> this._username;
  String get userRelationshipStatus => this._userRelationshipStatus;
}
class LikerData{
  List<dynamic> _likerList;
  bool _status;
  String _message;
  int _countLike;
  int _currentPage;
  String _nextPageUrl;
  LikerData(this._likerList,this._status,this._message,this._countLike);
  LikerData.map(dynamic obj){
    this._likerList=obj['result']['data'];
    this._status = obj['succes'];
    this._message = obj['msg'];
    this._countLike = obj['count_like'];
    this._currentPage = obj['result']['current_page'];
    this._nextPageUrl = obj['result']['next_page_url'];
  }
  LikerData.fromMap(Map<String,dynamic>obj)
  : this._likerList=obj['data'],
    this._status = obj['succes'],
    this._message = obj['msg'],
    this._countLike = obj['count_like'],
    this._currentPage = obj['result']['current_page'],
    this._nextPageUrl = obj['result']['next_page_url'];
  String get message => this._message;
  int get countLike => this._countLike;
  bool get status => this._status;
  List get likerList => this._likerList;
  String get nextPageUrl => this._nextPageUrl;
  int get currentPage => this._currentPage;
}
abstract class LikerRepository{
  Future<Liker> fetchLiker();
  Future<LikerData> fetchLikers(String token, String queryparam);
}