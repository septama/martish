import 'package:martish/data/fetchDataException.dart';
import 'package:martish/data/user/UserData.dart';
import 'package:martish/data/user/di.dart';

abstract class LoginContract{
  void onLoginSuccess(User user);
  void onLoginError(FetchDataException error);
}
class LoginPresenter{
  LoginContract _view;
  UserRepository _repository;

  LoginPresenter(this._view){
    _repository = new Injector().userRepository;
  }
  void login(String username,String password){
    _repository.
    login(username, password).then((data)=>_view.onLoginSuccess(data))
    .catchError((e)=>_view.onLoginError(new FetchDataException(e.toString())));
  }
}