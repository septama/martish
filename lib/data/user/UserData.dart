class User {
  String _username;
  String _jwtToken;
  int _id;
  String _profilePicture;
  User();

  User.map(dynamic obj) {
    this._username = obj["username"];
    this._jwtToken = obj["token"];
    this._profilePicture = obj["profile_picture"];
    this._id = obj["id"];
  }
  User.fromMap(Map<String, dynamic> obj)
  : this._username = obj["username"],
    this._jwtToken = obj["token"],
    this._profilePicture= obj["profile_picture"],
    this._id = obj["id"];

  User.fromDB(dynamic obj){
    this._username = obj["username"];
    this._jwtToken = obj["token"];
    this._profilePicture = obj["profile_picture"];
    this._id = obj["id"];
  }
  User.fromSql(dynamic obj){
    this._username = obj["username"];
    this._jwtToken = obj["jwtToken"];
    this._profilePicture = obj["profile_picture"];
    this._id = obj["id"];
  }
  String get username => _username;
  String get jwtToken => _jwtToken;
  String get profilePicture => _profilePicture;
  int get id => _id;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["username"] = _username;
    map["id"] = _id;
    map["jwtToken"] = _jwtToken;
    map['profile_picture'] = _profilePicture;
    return map;
  }
}
abstract class UserRepository{
  Future<List<User>> fetchUsers();
  Future<User> fromDB();
  Future<User> login(String username,String password);
  Future <User> refreshUser();
}
class DataUsers {
  List<dynamic> data;
  String message;
  bool status;

  DataUsers({this.data, this.message, this.status});

  DataUsers.fromMap(Map<String, dynamic> map)
      : data = map['data'],
        message = map['msg'],
        status = map['success'];
}