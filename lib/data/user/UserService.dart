import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:martish/data/database_helper.dart';
import 'package:martish/data/fetchDataException.dart';
import 'package:http/http.dart' as http;
import 'package:martish/data/config.dart';

import 'package:martish/data/user/UserData.dart';

class UserServiceRepository implements UserRepository{
  static Config config  = new Config();
  static String base_url =  config.host;
  static String prefix = config.prefix;
  @override
  Future<List<User>> fetchUsers() {
    // TODO: implement fetchUsers
    return null;
  }

  @override
  Future<User> fromDB() async{
    // TODO: implement fromDB
    var db = new DatabaseHelper();
    User user = User.fromDB(await db.getUser());
    return user;
  }

  @override
  Future<User> login(String username,String password) async{
    String url = base_url+prefix+'/login';
    http.Response response = await http.post(
      url,
      body: {'username':username,"password":password},
    );
    final statusCode = response.statusCode;
    final responseBody = json.decode(response.body);
    if(statusCode < 200 || statusCode > 400){
      throw new FetchDataException(
          "An error ocurred : [Status Code : $statusCode]");
    }
    // TODO: implement login
    return User.map(responseBody['user']);
  }
  @override 
  Future<User> refreshUser()async{
    var db = new DatabaseHelper();
    User user = User.fromSql(await db.getUser());
    String url = base_url+prefix+'/refresh';
    http.Response response = await http.post(
      url,
      headers: {
        HttpHeaders.authorizationHeader:"Bearer "+user.jwtToken,
        HttpHeaders.contentTypeHeader : 'application/x-www-form-urlencoded',
        }
    );
    final statusCode = response.statusCode;
    final responseBody = json.decode(response.body);
    if(statusCode < 200 || statusCode > 400){
      throw new FetchDataException(
          "An error ocurred : [Status Code : $statusCode]");
    }
    print(responseBody);
    return User.fromDB(responseBody['user']);
  }
  
}