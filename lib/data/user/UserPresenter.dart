import 'package:martish/data/fetchDataException.dart';
import 'package:martish/data/user/UserData.dart';
import 'package:martish/data/user/di.dart';

abstract class UserContract{
  void onRefreshUserSuccess(User user);
  void onRefreshUserError(FetchDataException error);
}
class UserPresenter{
  UserContract _view;
  UserRepository _repository;

  UserPresenter(this._view){
    _repository = new Injector().userRepository;
  }
  void refreshUser(){
    _repository.refreshUser().
    then((data)=> _view.onRefreshUserSuccess(data)).
    catchError((e)=>_view.onRefreshUserError(new FetchDataException(e.toString())));
  }
}