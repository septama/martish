import 'package:martish/data/fetchDataException.dart';
import 'package:martish/data/post/PostData.dart';
import 'package:martish/data/profile/ProfileData.dart';
import 'package:martish/data/user/UserData.dart';
import 'package:martish/data/profile/di.dart';

abstract class ProfileFriendContract{
  void onRefreshUserSuccess(User user);
  void onRefreshUserError(FetchDataException error);
  void onLoadPostSuccess(DataPost dataPost);
  void onLoadPostError(FetchDataException error);
  void onLoadProfileSuccess(UserProfileData userProfileData);
  void onLoadProfileError(FetchDataException error);
}
class ProfileFriendScreenPresenter{
  ProfileFriendContract _view;
  UserRepository _userRepository;
  PostRepository _postRepository;
  UserProfileRepository _userProfileRepository;
  ProfileFriendScreenPresenter(this._view){
    _userRepository = new Injector().userRepository;
    _userProfileRepository = new Injector().userProfileRepository;
    _postRepository = new Injector().postRepository;
  }
  void refreshUser(){
    _userRepository.refreshUser().
    then((data)=> _view.onRefreshUserSuccess(data)).
    catchError((e)=>_view.onRefreshUserError(new FetchDataException(e.toString())));
  } 
  void loadProfile(String token,int userId){
    _userProfileRepository.getUserProfile(token,userId).
    then((onValue)=>_view.onLoadProfileSuccess(onValue))
    .catchError((onError)=>_view.onLoadProfileError((new FetchDataException(onError.toString()))));
  }
}