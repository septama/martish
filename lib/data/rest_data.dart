import 'dart:async';
import 'dart:io';
import 'package:martish/model/user.dart';
import 'package:martish/model/user_post.dart';
import 'package:martish/util/network_util.dart';
class RestData{
  NetworkUtil _networkUtil = new NetworkUtil();
  static final _baseUrl = "http://192.168.43.85/alice/public/";
  static final _prefix = "api/v1";
  static final _loginUrl = _baseUrl+_prefix+"/login";
  static final _refreshUser = _baseUrl+_prefix+"/refresh";
  static final _feedUrl = _baseUrl + _prefix + "/post/feed";
  dynamic user;
  Future <User>login(String username,String password){
   return _networkUtil.post(
     _loginUrl,
     body:{"username":username,"password":password}
     ).then((dynamic res){print(res.toString());
       if(res['success']==false) {throw new Exception(res["msg"]);}
       return new User.map(res['user']);
     });
   }
   Future <User> refreshToken(String token)async{
     return _networkUtil.post(_refreshUser,
     headers: {
        HttpHeaders.authorizationHeader : 'Bearer '+token,
        HttpHeaders.contentTypeHeader : 'application/x-www-form-urlencoded'
     },
     ).then((dynamic res){print(res.toString());
      if(res['success']==false){throw new Exception(res['msg']);}
      print(res);
      return new User.map(res['user']);
     });
   }
   Future <List<UserPost>> getFeed(String token)async{
     return _networkUtil.get(
       _feedUrl,
      headers: {
        HttpHeaders.authorizationHeader : 'Bearer '+token,
        HttpHeaders.contentTypeHeader : 'application/x-www-form-urlencoded'
      },
     ).then((dynamic res){
       print(res.toString());
       if(res['success']==false){throw new Exception(res['msg']);}
       final DataUserPost _dataUserPost= new DataUserPost.fromMap(res);
       return _dataUserPost.data.map((userpost)=>new UserPost.fromMap(userpost)).toList();
     });
   }
}