class ActivityUser{
  int _activityId;
  int _userId;
  String _username;
  String _activityName;
  String _activityDesc;
  dynamic _activityData;
  String _timeText;
  int get activityId => _activityId;
  int get userId => _userId;
  String get username =>_username;
  String get activityName => _activityName;
  String get activityDesc =>_activityDesc;
  dynamic get activityData => _activityData;
  String  get timeText => _timeText;
  ActivityUser.map(dynamic obj){
    _activityId = obj['id'];
    _userId = obj['user_id'];
    _username = obj['username'];
    _activityName = obj['activity_name'];
    _activityDesc = obj['activity_desc'];
    _timeText = obj['time_text'];
    _activityData = obj['data'];
  }
  ActivityUser.fromMap(Map<String,dynamic> obj)
  :  _activityId = obj['id'],
    _userId = obj['user_id'],
    _username = obj['username'],
    _activityName = obj['activity_name'],
    _activityDesc = obj['activity_desc'],
    _timeText = obj['time_text'],
    _activityData = obj['data'];
}
class ActivityUserData{
  String _message;
  bool _success;
  List<dynamic> _activityUser;
  String _nextPageUrl;
  int _currentPage;
  String get message => _message;
  bool get success=>_success;
  List<dynamic> get activityUser => _activityUser;
  String get nextPageUrl => _nextPageUrl;
  int get currentPage =>_currentPage;
  ActivityUserData.map(dynamic obj){
    _message = obj['msg'];
    _success = obj['success'];
    _activityUser = obj['result']['data'];
    _nextPageUrl = obj['result']['next_page_url'];
    _currentPage = obj['result']['current_page']; 
  }
}
abstract class ActivityUserRepository{
  Future<ActivityUserData> getActivityUser(String token,String queryParam);
  Future<ActivityUserData> getFriendActivity(String token,String queryParam);
}