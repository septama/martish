import 'dart:convert';
import 'dart:io';

import 'package:martish/data/activity/ActivityData.dart';
import 'package:martish/data/config.dart';
import 'package:http/http.dart' as http;
import 'package:martish/data/fetchDataException.dart';

class ActivityUserServiceRepository implements ActivityUserRepository{
  static Config config  = new Config();
  static String base_url =  config.host;
  static String prefix = config.prefix;
  @override
  Future<ActivityUserData> getActivityUser(String token, String queryParam) async {
    // TODO: implement getActivityUser
    print(queryParam);
    print("Fetch activityUser");
    var url = "";
    if(queryParam !=null){
      url = base_url+prefix+'/activity'+queryParam;
    }
    else{
      url = base_url+prefix+'/activity';
    }
    http.Response response = await http.get(
      url,
      headers: {
        HttpHeaders.authorizationHeader : "Bearer "+token,
        HttpHeaders.contentTypeHeader : 'application/x-www-form-urlencoded',
      },
    );
    final statusCode = response.statusCode;
    final Map responseBody = json.decode(response.body);
    print(responseBody);
    final ActivityUserData _activityUserData = new ActivityUserData.map(responseBody);
    if(statusCode < 200 || statusCode > 400){
      throw new FetchDataException(
          "An error ocurred : [Status Code : $statusCode]");
    }
    print("Returning activity user data");
    return _activityUserData;
  }

  @override
  Future<ActivityUserData> getFriendActivity(String token, String queryParam) async{
    // TODO: implement getActivityUser
    print(queryParam);
    print("Fetch activityUser");
    var url = "";
    if(queryParam !=null){
      url = base_url+prefix+'/activity/user'+queryParam;
    }
    else{
      url = base_url+prefix+'/activity/user';
    }
    http.Response response = await http.get(
      url,
      headers: {
        HttpHeaders.authorizationHeader : "Bearer "+token,
        HttpHeaders.contentTypeHeader : 'application/x-www-form-urlencoded',
      },
    );
    final statusCode = response.statusCode;
    final Map responseBody = json.decode(response.body);
    print(responseBody);
    final ActivityUserData _activityUserData = new ActivityUserData.map(responseBody);
    if(statusCode < 200 || statusCode > 400){
      throw new FetchDataException(
          "An error ocurred : [Status Code : $statusCode]");
    }
    print("Returning activity user data");
    return _activityUserData;
  }
}