import 'package:martish/data/activity/di.dart';
import 'package:martish/data/user/UserData.dart';

import '../fetchDataException.dart';
import 'ActivityData.dart';

abstract class ListActivityFriendScreenContract{
  void onRefreshUserSuccess(User user);
  void onRefreshUserError(FetchDataException error);
  void onLoadActivitySuccess(ActivityUserData activityUserData);
  void onLoadActivityError(FetchDataException error);
}
class ListActivityFriendScreenPresenter{
  ListActivityFriendScreenContract _view;
  ActivityUserRepository _activityUserRepository;
  UserRepository _userRepository;
  ListActivityFriendScreenPresenter(this._view){
    _activityUserRepository = new Injector().activityUserRepository;
    _userRepository = new Injector().userRepository;
  }
  void loadActivity(String token, String queryParam){
    _activityUserRepository.getFriendActivity(token, queryParam)
    .then((onValue)=>_view.onLoadActivitySuccess(onValue))
    .catchError((onError) => _view.onLoadActivityError((FetchDataException(onError.toString()))));
  }
  void refreshUser(){
    _userRepository.refreshUser().
    then((data)=> _view.onRefreshUserSuccess(data)).
    catchError((e)=>_view.onRefreshUserError(new FetchDataException(e.toString())));
  } 
}