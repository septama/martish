import 'package:martish/data/activity/ActivityData.dart';
import 'package:martish/data/activity/ActivityService.dart';
import 'package:martish/data/user/UserData.dart';
import 'package:martish/data/user/UserService.dart';


class Injector {
  static final Injector _singleton = new Injector._internal();
  
    factory Injector() {
      return _singleton;
    }
  
    Injector._internal();

  ActivityUserRepository get activityUserRepository => new ActivityUserServiceRepository();
  UserRepository get userRepository => new UserServiceRepository();
}
