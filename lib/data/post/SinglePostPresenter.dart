import 'package:martish/data/fetchDataException.dart';
import 'package:martish/data/user/UserData.dart';

import 'PostData.dart';
import 'di.dart';

abstract class SinglePostScreenContract{
  void onRefreshUserSuccess(User user,String action);
  void onRefreshUserError(FetchDataException error);
  void onSendingLikeSuccess(String message, bool status);
  void onSendingLikeError(FetchDataException error);
}
class SinglePostScreenPresenter{
  SinglePostScreenContract _view;
  PostRepository _postRepository;
  UserRepository _userRepository;
  SinglePostScreenPresenter(this._view){
    _postRepository = new Injector().postRepository;
    _userRepository = new Injector().userRepository;
  }
  void refreshUser(String action){
    _userRepository.refreshUser().
    then((data)=> _view.onRefreshUserSuccess(data,action)).
    catchError((e)=>_view.onRefreshUserError(new FetchDataException(e.toString())));
  }
  void sendLike(String token, int post_id){
    _postRepository.sendLike(token, post_id)
    .then((onValue)=> _view.onSendingLikeSuccess(onValue['msg'], onValue['success']))
    .catchError((e)=>_view.onSendingLikeError(new FetchDataException(e.toString())));
  }
}