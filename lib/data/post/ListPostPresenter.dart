import 'package:martish/data/fetchDataException.dart';
import 'package:martish/data/post/PostData.dart';
import 'package:martish/data/user/UserData.dart';
import 'package:martish/data/post/di.dart';

abstract class ListPostContract{
  void onRefreshUserSuccess(User user,String action);
  void onRefreshUserError(FetchDataException error);
  void onLoadFeedSuccess(List<Post> listPost,String nextUrlPage);
  void onLoadFeedError(FetchDataException error);
  void onSendingLikeSuccess(String message, bool status);
  void onSendingLikeError(FetchDataException error);
}
class ListPostPresenter{
  ListPostContract _view;
  PostRepository _postRepository;
  UserRepository _userRepository;
  ListPostPresenter(this._view){
    _postRepository = new Injector().postRepository;
    _userRepository = new Injector().userRepository;
  }
  void refreshUser(String action){
    _userRepository.refreshUser().
    then((data)=> _view.onRefreshUserSuccess(data,action)).
    catchError((e)=>_view.onRefreshUserError(new FetchDataException(e.toString())));
  }
  void loadFeed(String token,String queryParam){
    _postRepository.fetchPosts(token,queryParam)
    .then((dataPost)=>_view.onLoadFeedSuccess(dataPost.post.map((f)=>Post.fromMap(f)).toList(),dataPost.next_page))
    .catchError((e)=>_view.onLoadFeedError(new FetchDataException(e.toString())));
  }
  void sendLike(String token, int post_id){
    _postRepository.sendLike(token, post_id)
    .then((onValue)=> _view.onSendingLikeSuccess(onValue['msg'], onValue['success']))
    .catchError((e)=>_view.onSendingLikeError(new FetchDataException(e.toString())));
  }
}