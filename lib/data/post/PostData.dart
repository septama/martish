import 'package:martish/data/place/PlaceData.dart';
import 'package:martish/data/user/UserData.dart';

class ContentPost{
  List <dynamic> _imageUrl;
  String _text;

  ContentPost(this._imageUrl,this._text);
  ContentPost.map(dynamic obj){
    this._imageUrl = obj['image'];
    this._text = obj['text'];
  }
  List <dynamic> get imageUrl => _imageUrl;
  String get text => _text;
}
class Post{
  int _id;
  int _user_id;
  String _username;
  String _profile_picture;
  ContentPost _content;
  DateTime _created_at;
  DateTime _updated_at;
  List<User> _likes;
  Place _place;
  String _place_name;
  String _place_id;
  int _countLike;
  int _countComment;
  int _isLike;
  Post();

  Post.map(dynamic obj){
    _id = obj['post_id'];
    _user_id = obj['user_id'];
    _content = new ContentPost.map(['content']);
    _username = obj['username'];
    _profile_picture = obj['profile_picture'];
    _created_at = DateTime.parse(obj['created_at']);
    _updated_at = DateTime.parse(obj['updated_at']);
    _countComment = obj['count_comment'];
    _countLike = obj['count_like'];
    _isLike = obj['is_liked'];
  }
  int get post_id => _id;
  String get profile_picture => _profile_picture;
  String get username => _username;
  String get place_name => _place_name;
  int get count_like => _countLike;
  int get count_comment => _countComment;
  Place get place => _place;
  int get is_like => _isLike;
  int get userId =>_user_id;
  void setIsLike(){
    if(this._isLike == 1){
      this._isLike = 0; this._countLike -1;
    }
    else{
      this._isLike = 1; this._countLike + 1;
    }
  }
  ContentPost get content => _content;
  Map <String,dynamic> toMap(){
    var obj = Map <String,dynamic>();
    obj['post_id']= _id;
    obj['user_id'] = _user_id;
    obj['content'] = _content;
    obj['username'] = _username;
    obj['profile_picture'] = _profile_picture;
    obj['created_at'] = _created_at;
    obj['updated_at'] = _updated_at;
    obj['place_name'] = _place_name;
    obj['place_id'] = _place_id;
    obj['count_comment'] = _countComment;
    obj['count_like'] = _countLike;
    obj['is_liked']=_isLike;
    return obj;
  }
  Post.fromMap(Map<String,dynamic>obj)
    : 
    _id = obj['post_id'],  
    _user_id = obj['user_id'],
    _content = new ContentPost.map(obj['content']),
    _username = obj['username'],
    _profile_picture = obj['profile_picture'],
    _created_at = DateTime.parse(obj['created_at']),
    _updated_at = DateTime.parse(obj['created_at']),
    _place = new Place.map(obj['places']),
    _countComment = obj['count_comment'],
    _countLike = obj['count_like'],
    _isLike = obj['is_liked'];
}
abstract class PostRepository{
  Future <DataPost> fetchPosts(String token,String queryparam);
  Future <Post> fetchPost();
  Future<DataPost> fetchPostsByUser(String token,String queryparam);
  Future<DataPost> fetchMyPost(String token,String queryparam);
  Future<dynamic> sendLike(String token, int post_id);
}
class DataPost{
   List<dynamic> post;
   dynamic data;
   String message;
   bool status;
   int count_data;
   String urlNextPage;
    DataPost({this.data, this.message, this.status});
    DataPost.map(dynamic obj){
      post = obj['result']['data'];
      data = obj['data'];
      message = obj['msg'];
      status = obj['success'];
      count_data = obj['count_data'];
      urlNextPage = obj['result']['next_page_url'];
    }
    String get next_page => this.urlNextPage;
    String get getMessage => this.message;
    bool get getStatus => this.status;
    dynamic get getData => this.data;
    List<dynamic> get getPost => this.post;
    DataPost.fromMap(Map<String, dynamic> map)
        : post = map['result']['data'],
          data = map['data'],
          message = map['msg'],
          status = map['success'],
          count_data = map['count_data'],
          urlNextPage = map['result']['next_page_url'];
}