import 'package:martish/data/fetchDataException.dart';
import 'package:martish/data/post/PostData.dart';
import 'package:martish/data/user/UserData.dart';

import 'di.dart';

abstract class FriendProfilePostContract{
  void onRefreshUserSuccess(User user);
  void onRefreshUserError(FetchDataException error);
  void onLoadFriendPostSuccess(DataPost postData);
  void onLoadFriendPostError(FetchDataException error);
}
class FriendProfilePostPresenter{
  FriendProfilePostContract _view;
  UserRepository _userRepository;
  PostRepository _postRepository;
  FriendProfilePostPresenter(this._view){
    this._userRepository = new Injector().userRepository;
    this._postRepository = new Injector().postRepository;
  }
  void refreshUser(){
    _userRepository.refreshUser().
    then((data)=> _view.onRefreshUserSuccess(data)).
    catchError((e)=>_view.onRefreshUserError(new FetchDataException(e.toString())));
  }
  void loadFriendPost(String token,String queryParam){
    _postRepository.fetchPostsByUser(token, queryParam)
    .then((onValue)=>_view.onLoadFriendPostSuccess(onValue))
    .catchError((onError)=>_view.onLoadFriendPostError(new FetchDataException(onError.toString())));
  }
}