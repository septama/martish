import 'dart:convert';
import 'dart:io';

import 'package:martish/data/fetchDataException.dart';
import 'package:http/http.dart' as http;
import 'package:martish/data/config.dart';

import 'package:martish/data/post/PostData.dart';
class PostServiceRepostory implements PostRepository{
  static Config config  = new Config();
  static String base_url =  config.host;
  static String prefix = config.prefix;
  @override
  Future<Post> fetchPost() {
    // TODO: implement fetchPost
    return null;
  }

  @override
  Future<DataPost> fetchPosts(String token,String queryparam) async{
    print("Fetch Post");
    // TODO: implement fetchPosts
    var url = "";
    if(queryparam !=null){
      url = base_url+prefix+'/post/feed'+queryparam;
    }
    else{
      url = base_url+prefix+'/post/feed';
    }
    http.Response response = await http.get(
      url,
      headers: {
        HttpHeaders.authorizationHeader : "Bearer "+token,
        HttpHeaders.contentTypeHeader : 'application/x-www-form-urlencoded',
      },
    );
    final statusCode = response.statusCode;
    final Map responseBody = json.decode(response.body);
    print(responseBody);
    final DataPost _dataPost = new DataPost.map(responseBody);
    if(statusCode < 200 || statusCode > 400){
      throw new FetchDataException(
          "An error ocurred : [Status Code : $statusCode]");
    }
    print(responseBody);
    return _dataPost;
  }

  @override
  Future<dynamic>sendLike(String token, int post_id) async{
    // TODO: implement sendLike
    print("Sending like..");
    var url =  base_url+prefix+'/post/like';
    http.Response response = await http.post(
      url,
      headers: {
        HttpHeaders.authorizationHeader : "Bearer "+token,
        HttpHeaders.contentTypeHeader : 'application/x-www-form-urlencoded',
      },
      body: { "post_id": post_id.toString() },
    );
    final statusCode = response.statusCode;
    final Map responseBody = json.decode(response.body);
    print(responseBody);
    if(statusCode < 200 || statusCode > 400){
      throw new FetchDataException(
          "An error ocurred : [Status Code : $statusCode]");
    }
    return responseBody;
  }

  @override
  Future<DataPost> fetchPostsByUser(String token, String queryparam) async {
    print("Fetch Other User Post");
    var url = "";
    if(queryparam !=null){
      url = base_url+prefix+'/post/user/other'+queryparam;
    }
    else{
      url = base_url+prefix+'/post/user/other';
    }
    http.Response response = await http.get(
      url,
      headers: {
        HttpHeaders.authorizationHeader : "Bearer "+token,
        HttpHeaders.contentTypeHeader : 'application/x-www-form-urlencoded',
      },
    );
    final statusCode = response.statusCode;
    final Map responseBody = json.decode(response.body);
    print(responseBody);
    final DataPost _dataPost = new DataPost.map(responseBody);
    if(statusCode < 200 || statusCode > 400){
      throw new FetchDataException(
          "An error ocurred : [Status Code : $statusCode]");
    }
    print(responseBody);
    return _dataPost;
  }

  @override
  Future<DataPost> fetchMyPost(String token, String queryparam) async {
    // TODO: implement fetchMyPost
    print("Fetch My Post");
    var url = "";
    if(queryparam !=null){
      url = base_url+prefix+'/post/user'+queryparam;
    }
    else{
      url = base_url+prefix+'/post/user';
    }
    http.Response response = await http.get(
      url,
      headers: {
        HttpHeaders.authorizationHeader : "Bearer "+token,
        HttpHeaders.contentTypeHeader : 'application/x-www-form-urlencoded',
      },
    );
    final statusCode = response.statusCode;
    final Map responseBody = json.decode(response.body);
    print(responseBody);
    final DataPost _dataPost = new DataPost.map(responseBody);
    if(statusCode < 200 || statusCode > 400){
      throw new FetchDataException(
          "An error ocurred : [Status Code : $statusCode]");
    }
    print(responseBody);
    return _dataPost;
  }
}
