import 'package:martish/data/fetchDataException.dart';
import 'package:martish/data/post/PostData.dart';
import 'package:martish/data/user/UserData.dart';

import 'di.dart';

abstract class ProfilePostContract{
  void onRefreshUserSuccess(User user);
  void onRefreshUserError(FetchDataException error);
  void onLoadMyPostSuccess(DataPost postData);
  void onloadMyPostError(FetchDataException error);
}
class ProfilePostPresenter{
  ProfilePostContract _view;
  UserRepository _userRepository;
  PostRepository _postRepository;
  ProfilePostPresenter(this._view){
    this._userRepository = new Injector().userRepository;
    this._postRepository = new Injector().postRepository;
  }
  void refreshUser(){
    _userRepository.refreshUser().
    then((data)=> _view.onRefreshUserSuccess(data)).
    catchError((e)=>_view.onRefreshUserError(new FetchDataException(e.toString())));
  }
  void loadMyPost(String token,String queryParam){
    _postRepository.fetchMyPost(token, queryParam)
    .then((onValue)=>_view.onLoadMyPostSuccess(onValue))
    .catchError((onError)=>_view.onloadMyPostError(new FetchDataException(onError.toString())));
  }
}