class Config{
  String _developmentHost = 'http://192.168.43.85/alice/public';
  String _productionHost = 'https://api.hailup.com';
  String _prefix = '/api/v1';
  String get developmentHost => this._developmentHost;
  String get productionHost => this._productionHost;
  String get host => this._developmentHost;
  String get prefix =>this._prefix;
}