import 'dart:async';
import 'dart:io' as io;

import 'package:path/path.dart';
import 'package:martish/data/user/UserData.dart';
import 'package:martish/model/userRegister.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class DatabaseHelper{
  static final DatabaseHelper _instance = new DatabaseHelper.internal();
  factory DatabaseHelper() => _instance;

  static Database _db;

  Future<Database> get db async {
    if(_db != null)
      return _db;
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "main.db");
    var theDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    return theDb;
  }
  

  void _onCreate(Database db, int version) async {
    // When creating the db, create the table
    await db.execute(
    "CREATE TABLE User(id INTEGER PRIMARY KEY, username TEXT, email TEXT,profile_picture TEXT, jwtToken TEXT)");
    print("Created tables User");
    await db.execute(
    "CREATE TABLE UserRegister(email TEXT PRIMARY KEY,verified_code TEXT)");
    print("Created tables User Register");
  }

  Future<int> saveUser(User user) async {
    var dbClient = await db;
    int res = await dbClient.insert("User", user.toMap());
    return res;
  }
  Future<int> saveUserRegister(UserRegister user) async {
    var dbClient = await db;
    int res = await dbClient.insert("UserRegister", user.toMap());
    return res;
  }

  Future<int> deleteUsers() async {
    var dbClient = await db;
    int res = await dbClient.delete("User");
    return res;
  }

  Future<bool> isLoggedIn() async {
    var dbClient = await db;
    var res = await dbClient.query("User");
    return res.length > 0? true: false;
  }
  Future <bool> updateToken(User user) async{
    var dbClient = await db;
    var result = await dbClient.rawUpdate('UPDATE User SET jwtToken = ? , username = ? WHERE username = ?',[user.jwtToken,user.username,user.username]);
    return result == 1 ? true:false;
  }
  Future <Map> getUser()async{
    var dbClient =  await db;
    var res = await dbClient.rawQuery("Select * from User");
    return res.first;
  }
}