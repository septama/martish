import 'dart:convert';
import 'dart:io';

import 'package:martish/data/comment/CommentData.dart';
import 'package:martish/data/config.dart';
import 'package:martish/data/fetchDataException.dart';
import 'package:http/http.dart' as http;

class CommentServiceRepository implements CommentRepository{
  static Config config  = new Config();
  static String base_url =  config.host;
  static String prefix = config.prefix;
  
  @override
  Future<CommentData> fetchComment() {
    // TODO: implement fetchComment
    return null;
  }

  @override
  Future<CommentData> fetchComments(String token, String queryParam) async{
    // TODO: implement fetchComments
    print(queryParam);
    print("Fetch comments");
    var url = "";
    if(queryParam !=null){
      url = base_url+prefix+'/post/comment'+queryParam;
    }
    else{
      url = base_url+prefix+'/post/comment';
    }
    http.Response response = await http.get(
      url,
      headers: {
        HttpHeaders.authorizationHeader : "Bearer "+token,
        HttpHeaders.contentTypeHeader : 'application/x-www-form-urlencoded',
      },
    );
    final statusCode = response.statusCode;
    final Map responseBody = json.decode(response.body);
    print(responseBody);
    final CommentData _commentData = new CommentData.map(responseBody);
    if(statusCode < 200 || statusCode > 400){
      throw new FetchDataException(
          "An error ocurred : [Status Code : $statusCode]");
    }
    print("Returning comment data");
    return _commentData;
  }

  @override
  Future sendComment(String token, int _postId, String _text) async{
    // TODO: implement sendComment
    print("Sending like..");
    var url =  base_url+prefix+'/post/comment/add';
    http.Response response = await http.post(
      url,
      headers: {
        HttpHeaders.authorizationHeader : "Bearer "+token,
        HttpHeaders.contentTypeHeader : 'application/x-www-form-urlencoded',
      },
      body: { "post_id": _postId.toString(), "text":_text },
    );
    final statusCode = response.statusCode;
    final Map responseBody = json.decode(response.body);
    print(responseBody);
    if(statusCode < 200 || statusCode > 400){
      throw new FetchDataException(
          "An error ocurred : [Status Code : $statusCode]");
    }
    return responseBody;
  }
}