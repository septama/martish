import 'package:martish/data/comment/CommentData.dart';
import 'package:martish/data/comment/di.dart';
import 'package:martish/data/fetchDataException.dart';
import 'package:martish/data/user/UserData.dart';

abstract class CommentContract{
  void onLoadCommentSuccess(List<Comment> commentList,int current_page,String url_next_page);
  void onLoadCommentError(FetchDataException error);
  void onRefreshUserSuccess(User user);
  void onRefreshUserError(FetchDataException error);
  void onSendCommentSucces(String message,bool success,dynamic result);
  void onSendCommentError(FetchDataException error);
}
class CommentPresenter{
  CommentContract _view;
  CommentRepository _commentRepository;
  UserRepository _userRepository;
  CommentPresenter(this._view){
    _commentRepository = new Injector().commentRepository;
    _userRepository = new Injector().userRepository;
  }
  void sendComment(String token, int _postId, String _text){
    _commentRepository.sendComment(token, _postId, _text)
    .then((onValue)=>_view.onSendCommentSucces(onValue['msg'],onValue['success'], onValue['result']))
    .catchError((onError)=>_view.onSendCommentError(new FetchDataException(onError.toString())));
  }
  void loadComment(String token,String queryParam){
    _commentRepository.fetchComments(token,queryParam).
    then((commentData)=>_view.onLoadCommentSuccess(
      commentData.commentList.map((f)=>Comment.fromMap(f)).toList(), 
        commentData.currentPage, 
        commentData.urlNextPage
    )).catchError((e)=> _view.onLoadCommentError(new FetchDataException(e.toString())));
  }
  void refreshUser(){
    _userRepository.refreshUser().
    then((data)=> _view.onRefreshUserSuccess(data)).
    catchError((e)=>_view.onRefreshUserError(new FetchDataException(e.toString())));
  }
}