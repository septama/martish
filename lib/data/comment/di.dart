import 'package:martish/data/comment/CommentData.dart';
import 'package:martish/data/comment/CommentService.dart';
import 'package:martish/data/user/UserData.dart';
import 'package:martish/data/user/UserService.dart';


class Injector {
  static final Injector _singleton = new Injector._internal();
  
    factory Injector() {
      return _singleton;
    }
  
    Injector._internal();

  CommentRepository get commentRepository => new CommentServiceRepository();
  UserRepository get userRepository => new UserServiceRepository();
}
