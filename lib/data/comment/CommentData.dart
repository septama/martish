class Content{
  String _text;
  Content(this._text);
  Content.map(dynamic obj){
    this._text = obj['text'];
  }
  Content.fromMap(Map<String,dynamic> obj)
  : this._text = obj['text'];

  String get text => this._text;
}
class Comment{
  Content _content;
  String _profilePicture;
  String _username;
  int _userId;
  String _createdAt;
  Comment(this._content,this._profilePicture,this._username,this._userId,this._createdAt);
  Comment.map(dynamic obj){
    this._content = Content.map(obj['content']);
    this._profilePicture = obj['profile_picture'];
    this._username = obj['username'];
    this._userId = obj['user_id'];
    this._createdAt = obj['created_at'];
  }
  String get username =>_username;
  String get profile_picture => _profilePicture;
  Content get content => _content;
  int get user_id => _userId;
  String get createdAt => _createdAt;
  Comment.fromMap(Map<String,dynamic>obj)
  : this._content = Content.map(obj['content']),
    this._profilePicture = obj['profile_picture'],
    this._username = obj['username'],
    this._userId = obj['user_id'],
    this._createdAt = obj['created_at'];  
}
class CommentData{
  List<dynamic> _commentList;
  bool _status;
  String _message;
  int _countComment;
  int _currentPage;
  String _urlNextPage;
  CommentData(this._commentList,this._status,this._countComment,this._message);
  CommentData.map(dynamic obj){
    this._commentList = obj['result']['data'];
    this._currentPage = obj['result']['current_page'];
    this._message = obj['msg'];
    this._countComment = obj['count_comment'];
    this._status = obj['success'];
    this._urlNextPage = obj['result']['next_page_url'];
  }
  List get commentList => this._commentList;
  String get urlNextPage =>this._urlNextPage;
  int get currentPage => this._currentPage;
  CommentData.fromMap(Map<String,dynamic> obj)
  : this._commentList = obj['result']['data'],
    this._currentPage = obj['result']['current_page'],
    this._message = obj['msg'],
    this._countComment = obj['count_comment'],
    this._status = obj['success'],
    this._urlNextPage = obj['result']['next_page_url'];
}
abstract class CommentRepository{
  Future<CommentData>fetchComments(String token,String queryParam);
  Future<dynamic> sendComment(String token, int _postId, String _text);
  Future<CommentData>fetchComment();
}