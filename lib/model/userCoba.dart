import 'post.dart';
import 'package:flutter/material.dart';

class User {
  String _username;
  String get username => _username;
  List<Post> posts;
  AssetImage profilePicture;
  List<User> followers;
  List<User> following; 
  List<Post> savedPosts;
  String location;
  String coment;
  String email;
  String id;
  
  User(this._username,this.profilePicture,this.location,this.coment, this.followers, this.following);
  }