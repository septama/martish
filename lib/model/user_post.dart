import 'dart:convert';
import 'comment.dart';
import 'user.dart';

class UserPost{
  BigInt _id;
  BigInt _user_id;
  String _username;
  String _profile_picture;
  JsonCodec _content;
  DateTime _created_at;
  DateTime _updated_at;
  List<User> _likes;
  List<Comment> _comments;
  JsonCodec _place;
  BigInt _countLike;
  BigInt _countComment;
  bool _isLiked;
  bool _isSaved;
  UserPost();

  UserPost.map(dynamic obj){
    _id = obj['id'];
    _user_id = obj['user_id'];
    _content = obj['content'];
    _username = obj['username'];
    _profile_picture = obj['profile_picture'];
    _created_at = obj['created_at'];
    _updated_at = obj['updated_at'];
    _place = obj['place'];
    _countComment = obj['count_comment'];
    _countLike = obj['count_like'];
  }
  Map <String,dynamic> toMap(){
    var obj = Map <String,dynamic>();
    obj['id']= _id;
    obj['user_id'] = _user_id;
    obj['content'] = _content;
    obj['username'] = _username;
    obj['profile_picture'] = _profile_picture;
    obj['created_at'] = _created_at;
    obj['updated_at'] = _updated_at;
    obj['place'] = _place;
    obj['count_comment'] = _countComment;
    obj['count_like'] = _countLike;
    obj['isLiked'] = _isLiked;
    obj['isSaved'] = _isSaved;
    return obj;
  }
  UserPost.fromMap(Map<String,dynamic>obj)
    :_id = obj['id'],
    _user_id = obj['user_id'],
    _content = obj['content'],
    _username = obj['username'],
    _profile_picture = obj['profile_picture'],
    _created_at = obj['created_at'],
    _updated_at = obj['updated_at'],
    _place = obj['place'],
    _countComment = obj['count_comment'],
    _countLike = obj['count_like'];
}
class DataUserPost{
   List<dynamic> data;
   String message;
   bool status;

    DataUserPost({this.data, this.message, this.status});

    DataUserPost.fromMap(Map<String, dynamic> map)
        : data = map['data'],
          message = map['msg'],
          status = map['success'];
}