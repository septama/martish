class User {
  String _username;
  String _jwtToken;
  int _id;
  User();

  User.map(dynamic obj) {
    this._username = obj["username"];
    this._jwtToken = obj["token"];
    this._id = obj["id"];
  }
  User.refresh(dynamic obj){
    this._username = obj["username"];
    this._jwtToken = obj["token"];
  }
  User.fromDB(dynamic obj){
    this._username = obj["username"];
    this._jwtToken = obj["jwtToken"];
    this._id = obj["id"];
  }
  String get username => _username;
  String get jwtToken => _jwtToken;
  int get id => _id;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["username"] = _username;
    map["id"] = _id;
    map["jwtToken"] = _jwtToken;
    return map;
  }
  
}