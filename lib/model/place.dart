import 'dart:convert';

class Place{
  String _longitude;
  String _latitude;
  String _place_id;
  String _place_name;
  JsonCodec _photo;
  String _icon; 
  JsonCodec _type;
  String _address_detail;
  BigInt _user_rating_total;
  double _rating;
  JsonCodec _opening_hours;

  Place (this._latitude,this._longitude,
  this._place_id,this._place_name,this._photo,
  this._icon,this._type,this._address_detail,
  this._user_rating_total,this._rating,
  this._opening_hours);
  Place.map(dynamic obj){
    this._latitude = obj['latitude'];
    this._longitude = obj['longitude'];
    this._place_id = obj['place_id'];
    this._place_name = obj['place_name'];
    this._photo = obj['photo'];
    this._icon = obj['icon'];
    this._type = obj['type'];
    this._address_detail = obj['address_detail'];
    this._user_rating_total = obj['user_rating_total'];
    this._rating = obj['rating'];
    this._opening_hours = obj['opening_hours'];
  }
  String get latitude => _latitude;
  String get longitude => _longitude;
  String get place_id => _place_id;
  String get place_name => _place_name;
  JsonCodec get photo => _photo;
  String get icon => _icon;
  JsonCodec get type => _type;
  String get address_detail => _address_detail;
  BigInt get user_rating_total => _user_rating_total;
  double get rating => _rating;
  JsonCodec get opening_hours => _opening_hours;


  Map<String,dynamic> map(){
    var obj = new Map<String, dynamic>();
    obj['latitude'] = _latitude;
    obj['longitude'] = _longitude;
    obj['place_id'] = _place_id;
    obj['place_name'] = _place_name;
    obj['photo'] = _photo;
    obj['icon'] = _icon;
    obj['type'] = _type;
    obj['address_detail'] = _address_detail;
    obj['user_rating_total'] = _user_rating_total;
    obj['rating'] = _rating;
    obj['opening_hours'] = _opening_hours;
    return obj;
  }
}
