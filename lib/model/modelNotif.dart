import 'userCoba.dart';

class ModelNotif {
  String notifLiked;
  String notifComment;
  User user;
  DateTime dateOfnotif;
  bool isLiked;
  List<User> likes;
  ModelNotif(this.user, this.notifLiked,this.notifComment, this.dateOfnotif, this.isLiked, this.likes);
}