class UserRegister {
  String _email;
  String _verified_code;
  UserRegister(this._email,this._verified_code);

  UserRegister.map(dynamic obj) {
    this._email = obj['email'];
    this._verified_code = obj['verified_code'];
  }
  String get email => _email;
  String get verified_code => _verified_code;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map['email'] = _email;
    map['verified_code'] = _verified_code;
    return map;
  }
}