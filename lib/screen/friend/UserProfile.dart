import 'package:flutter/material.dart';
import 'package:martish/data/profile/ProfileData.dart';
import 'package:martish/screen/profile/editProfile.dart';

class FriendProfileScreen extends StatefulWidget {
  final UserProfile userProfile;
  FriendProfileScreen({@required this.userProfile});
  @override
  _FriendProfileScreenState createState() => _FriendProfileScreenState();
}

class _FriendProfileScreenState extends State<FriendProfileScreen>{
  UserProfile _userProfile;
  bool _loadScreen = true;
  final GlobalKey _key = new GlobalKey();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this._userProfile = this.widget.userProfile;
    if(this._userProfile!=null){
      this._loadScreen=false;
    }
  }
  @override
  Widget build(BuildContext context) {
    if(this._loadScreen){
      return SizedBox(width: 50, child: CircularProgressIndicator());
    }
    else{
      return Column(
        key: this._key,
        children: <Widget>[
          this.rowOne(),
          this.rowTwo(),
          SizedBox(height: 15.0),
          this.rowThree(),
          SizedBox(height: 15.0),
          this.rowFour(),
          SizedBox(height: 15.0),
          this._btnEditProfile(),
        ],
      );
    }
  }
   Widget _btnEditProfile(){
    Color colorBtn;
    Text textBtn;
    if(this.widget.userProfile.userRelationship==null){
      colorBtn = Colors.blue;
      textBtn = Text("Follow",
      style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  fontFamily: 'Montserrat'));
    }
    else if(this.widget.userProfile.userRelationship=='following'){
      colorBtn = Colors.white;
      textBtn = Text("following", style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontFamily: 'Montserrat'));
    }
    else{
      colorBtn = Colors.blue;
      textBtn = Text("pending", style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontFamily: 'Montserrat'));
    }
    return Container(
      height: 40.0,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Color(0xfff4f4f4)),
        ),
        color: colorBtn,
        onPressed: (){
         
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Center(
                child: textBtn,
                  )
                ],
              ),
          )
    );
  }
  Widget rowThree(){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          InkWell(
            onTap: (){

            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(this._userProfile.countPost.toString(),style: TextStyle(color:Colors.black,fontWeight: FontWeight.bold)),
                Text('post',style: TextStyle(color:Colors.black)),
                  ],
              ),
          ),
          InkWell(
            onTap: (){},
            child:Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(this._userProfile.countFollower.toString(),style: TextStyle(color:Colors.black,fontWeight: FontWeight.bold)),
                Text('follower',style: TextStyle(color:Colors.black)),
                ],
            ),
          ),
          InkWell(
            onTap: (){},
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(this._userProfile.countFollowing.toString(),style: TextStyle(color:Colors.black,fontWeight: FontWeight.bold)),
                Text('following',style: TextStyle(color:Colors.black)),
              ],
            ),
          )
          ],
        ),
      );
  }
  Widget rowFour(){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      alignment: Alignment.center,
      child: Text(this._userProfile.bio == null ? "" : this._userProfile.bio,style: TextStyle(fontSize: 18.0))
    );
  }
  Widget rowTwo(){
    return Container(
      alignment: Alignment.bottomCenter,
      height: 130.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(this._userProfile.fullName == null ? "" :  this._userProfile.fullName ,style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 28.0),
            ),
            SizedBox(width: 5.0),
            ],
        )
      );
  }
  Widget rowOne(){
    return Container(
      child: Stack(
        alignment: Alignment.bottomCenter,
        overflow: Overflow.visible,
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  height: 200.0,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(this._userProfile.bannerPicture)
                    )
                  ),
                ),
              )
            ],
          ),
          Positioned(
            top: 100.0,
            child: Container(
                   height: 190.0,
                   width: 190.0,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                        fit: BoxFit.cover,
                          image: NetworkImage(this._userProfile.profilePicture),
                        ),
                        border: Border.all(
                          color: Colors.white,
                          width: 6.0
                        )
                    ),
                  ),
            ),
          ],
        ),
    );
  }
}