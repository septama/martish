import 'package:flutter/material.dart';
import 'package:martish/data/database_helper.dart';
import 'package:martish/data/fetchDataException.dart';
import 'package:martish/data/friend/ProfileFriendPresenter.dart';
import 'package:martish/data/post/PostData.dart';
import 'package:martish/data/profile/ProfileData.dart';
import 'package:martish/data/user/UserData.dart';
import 'package:martish/screen/activity/ListFriendActivity.dart';
import 'package:martish/screen/post/ListFriendPost.dart';
import 'package:martish/screen/friend/UserProfile.dart';

class FriendProfile extends StatefulWidget {
  final int userId;
  final String username;
  FriendProfile({@required this.userId,@required this.username});
  @override
  _FriendProfileState createState() => _FriendProfileState();
}

class _FriendProfileState extends State<FriendProfile> implements ProfileFriendContract{
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  User _modelUser;
  UserProfile _userProfile;
  bool _showListPost = true;
  bool _showListActivity = false;
  int _currentScreenList = 0;
  bool _loadScreen = true;
  ProfileFriendScreenPresenter _presenter;
  _FriendProfileState(){
    _presenter = new ProfileFriendScreenPresenter(this);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        title: Text(this.widget.username),
        automaticallyImplyLeading: true,
      ),
      body: ListView(
        children: <Widget>[
          this._main()
        ],
      ),
    );
  }
  Widget _main(){
    if(this._userProfile==null){
      return SizedBox(width: 50, child: CircularProgressIndicator());
    }
    else{
      return Column(
            children: <Widget>[
              FriendProfileScreen(userProfile: this._userProfile),
              SizedBox(height: 15.0),
              this._buttonPostOrActivity(),
              this._userMedia(),
            ],
        );
    }
  }
   Widget _userMedia(){
    if(this._userProfile.userRelationship != 'following'){
      return Container(
        color:Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.lock),
            Text("Akun ini bersifat privat",style: TextStyle(fontSize: 16)),
          ],
        ),
      );
    }
    else{
     List<Widget> _bottomWidget = [
        ListFriendPost(userId: this.widget.userId),
        ListFriendActivity(userId: this.widget.userId),
      ];
     return IndexedStack(
        index: this._currentScreenList,
        children: _bottomWidget,
      );
    }
  }
  Widget _buttonPostOrActivity(){
    if(this._userProfile.userRelationship !='following'){
     return SizedBox(height: 0);      
    }
    else{
      return Container(
        padding: EdgeInsets.all(0),
        decoration: BoxDecoration(
          border: Border.all(color: Color(0xffe3e1e1)),
          color: Colors.white,
          ),
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 5,
                child: Container(
                  height: 60,
                  child: new Material(
                    child:IconButton(
                      onPressed: (){
                        setState(() {
                          this._currentScreenList = 0;
                          this._showListPost = !this._showListPost;
                          this._showListActivity = !this._showListActivity;
                        });
                      },
                      icon: Icon(
                        Icons.collections,
                        color: this._showListPost ? Colors.blueAccent : Colors.black ,),
                      )
                  ),
                ),
              ),
              Expanded(
                flex: 5,
                child: Container(
                  height: 60,
                  child: new Material(
                    child:IconButton(
                      onPressed: (){
                        setState(() {
                          this._currentScreenList=1;
                          this._showListPost = !this._showListPost;
                          this._showListActivity = !this._showListActivity;
                        });
                      },
                      icon: Icon(
                      Icons.list,
                      color: this._showListActivity? Colors.blueAccent : Colors.black),
                      )
                    ),
                  ),
                )
            ],
        ),
      );
    }
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _presenter.refreshUser();
  }
  @override
  void onLoadPostError(FetchDataException error) {
    // TODO: implement onLoadPostError
  }

  @override
  void onLoadPostSuccess(DataPost dataPost) {
    // TODO: implement onLoadPostSuccess
  }

  @override
  void onLoadProfileError(FetchDataException error) {
    // TODO: implement onLoadProfileError
    print(error.toString());
  }

  @override
  void onLoadProfileSuccess(UserProfileData userProfileData) {
    // TODO: implement onLoadProfileSuccess
    if(userProfileData.success){
      print("Load profile success");
      setState(() {
          this._userProfile = userProfileData.userProfile;
          this._loadScreen=false;
        });
      print("Full Name"+this._userProfile.fullName);
    } else{
      setState(() {
        this._loadScreen = true;
      });
      Scaffold.of(context).showSnackBar(new SnackBar(content: Text("Something wrong with server")));
    }
  }

   @override
  void onRefreshUserError(FetchDataException error) {
    // TODO: implement onRefreshUserError
    print(error.toString());
  }

  @override
  void onRefreshUserSuccess(User user) async{
    // TODO: implement onRefreshUserSuccess
  setState(() {
      this._modelUser = user;
    });
    print(this._modelUser.username);
    var db  = new DatabaseHelper();
    print(this._modelUser.jwtToken);
    bool updateToken = await db.updateToken(this._modelUser);
    if(updateToken){
      print("Loading Feed");
      _presenter.loadProfile(this._modelUser.jwtToken,this.widget.userId);  
    }
    else{
      print("Update Token Error");
      Scaffold.of(context).showSnackBar(new SnackBar(content: Text("Something went wrong")));
    }
  }
}