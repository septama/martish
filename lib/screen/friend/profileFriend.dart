import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:martish/component/profileComponent.dart';
class ProfileFriendScreen extends StatefulWidget {
  @override
  _ProfileFriendScreenState createState() => _ProfileFriendScreenState();
}

class _ProfileFriendScreenState extends State<ProfileFriendScreen> {
  bool pressedBtnFollow=false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: null,
      body: ListView(
        children: <Widget>[
          new Column(
            children: <Widget>[
              ProfileComponent().profileAndBannerPicture,
              ProfileComponent().userName,
              SizedBox(height: 15.0),
              ProfileComponent().userBio,
              SizedBox(height: 15.0),
              ProfileComponent().userPostAndFollow,
              SizedBox(height: 15.0),
              BtnFollowFriend(),
              Container(
                child: Column(
                  children: <Widget>[
                    Row()
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}