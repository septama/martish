import 'dart:async';

import 'package:flutter/material.dart';
import 'package:martish/data/auth.dart';
import 'package:martish/data/database_helper.dart';
import 'package:martish/data/fetchDataException.dart';
import 'package:martish/data/like/LikeData.dart';
import 'package:martish/data/like/LikePresenter.dart';
import 'package:martish/data/user/UserData.dart';
import 'package:martish/data/user/UserPresenter.dart';
import 'package:martish/screen/friend/profile.dart';

class ListLikerScreen extends StatefulWidget {
  final int post_id;
  ListLikerScreen({@required this.post_id});
  @override
  _ListLikerScreenState createState() => _ListLikerScreenState();
}

class _ListLikerScreenState extends State<ListLikerScreen> implements LikerContract,AuthStateListener,UserContract{
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();
  ScrollController _scrollController = new ScrollController();
  LikerPresenter _likerPresenter;
  UserPresenter _userPresenter;
  User _userModel;
  bool _refresh=false;
  String _nextPageUrl = "";
  int _currentPage=1;
  List<Liker> _likerList = new List();
  _ListLikerScreenState(){
   _likerPresenter = new LikerPresenter(this);
   _userPresenter = new UserPresenter(this);
   var authStateProvider = new AuthStateProvider();
   authStateProvider.subscribe(this);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        automaticallyImplyLeading: true,
        title: Text("Orang yang menyukai"),
      ),
      key: this._scaffoldKey,
      body: this._buildLikerList(this._likerList),
    );
  }
  Widget _buildLikerList(List<Liker> likerList){
    return RefreshIndicator(
      key: _refreshIndicatorKey,
      onRefresh: _onRefresh,
      child: ListView.builder(
        controller: this._scrollController,
        itemCount: likerList.length,
        itemBuilder: (BuildContext context, int index){
          return _liker(likerList[index]);
        },
      ),
    );
  }
  Widget _liker(Liker liker){
    return new Container(
      padding: EdgeInsets.symmetric(horizontal: 15,vertical: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          InkWell(
            onTap: (){
              Navigator.of(context).push(new MaterialPageRoute(
                builder: (BuildContext context)=>FriendProfile(
                  userId: liker.userId,
                  username: liker.username,
                )
              ));
            },
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  CircleAvatar(
                    backgroundImage: NetworkImage(liker.profilePicture),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child:Text(liker.username, style: TextStyle(fontWeight: FontWeight.bold),)
                    ),
                ],
              ),
            ),
          ),
          FlatButton(
            onPressed: (){
              
            },
            color:Colors.blueAccent,
            textColor: Colors.white,
            child: Text(liker.userRelationshipStatus == null ? "Follow" : liker.userRelationshipStatus),
          )
        ],
      ),
    );
  }
  Future<Null> _onRefresh(){
    Completer<Null> completer = new Completer<Null>();

    new Timer(new Duration(seconds: 3), () {
      setState(() {
        this._refresh=true;
      });
      print("timer complete");
      completer.complete();
    });
    return completer.future;
  }
  void getMoreData()async{
    this._userPresenter.refreshUser();
  }
  @override
  void initState() {
    // TODO: implement initState
    this.getMoreData();
    super.initState(); 
    this._scrollController.addListener((){
      if(this._scrollController.position.pixels==this._scrollController.position.maxScrollExtent){
        if(this._nextPageUrl!=null){
          this.getMoreData();
        }
      }
    });
  }
  @override
  void onLoadLikerError(FetchDataException error) {
    // TODO: implement onLoadLikerError
    print("On load liker error :" +error.toString());
  }

  @override
  void onLoadLikerSuccess(List<Liker> liker, int currentPage, String nextPageUrl) {
    // TODO: implement onLoadLikerSuccess
    if(liker.length != 0){
      if(this._likerList.length ==0){
        setState(() {
          this._likerList = liker;
          this._currentPage = currentPage + 1;
          this._nextPageUrl = nextPageUrl;
        });
      }
      else{
        setState(() {
          this._likerList..addAll(liker);
          this._currentPage = currentPage + 1;
          this._nextPageUrl = nextPageUrl;
        });
      }
    }
    else{
      this._scaffoldKey.currentState.showSnackBar(new SnackBar(content: Text("Tidak yang menyukai postingan ini")));
    }
  }

  @override
  void onAuthStateChanged(AuthState state) {
    // TODO: implement onAuthStateChanged
  }

  @override
  void onRefreshUserError(FetchDataException error) {
    // TODO: implement onRefreshUserError
    print("On refresh user error :" + error.toString());
  }

  @override
  void onRefreshUserSuccess(User user) async{
    // TODO: implement onRefreshUserSuccess
    setState(() {
      this._userModel = user;
    });
    print(this._userModel.username);
    var db  = new DatabaseHelper();
    print(this._userModel.jwtToken);
    bool updateToken = await db.updateToken(this._userModel);
    if(updateToken){
      print("Loading Liker");
      if(this._refresh){
        setState(() {
          this._likerList = new List();
          this._refresh=false;
        });
      }
      if(this._nextPageUrl != null){
        String param = 
        "?post_id="+this.widget.post_id.toString();
        _likerPresenter.loadLiker(this._userModel.jwtToken,param);
      }
    }
    else{
      print("Update Token Error");
      this._scaffoldKey.currentState.showSnackBar(new SnackBar(content: Text("Something went wrong"),));
    }
  }
}