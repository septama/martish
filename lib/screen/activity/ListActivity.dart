import 'package:flutter/material.dart';
import 'package:martish/data/activity/ActivityData.dart';
import 'package:martish/data/activity/ActivityListPresenter.dart';
import 'package:martish/data/database_helper.dart';
import 'package:martish/data/fetchDataException.dart';
import 'package:martish/data/user/UserData.dart';

class ListActivityScreen extends StatefulWidget {
  @override
  _ListActivityScreenState createState() => _ListActivityScreenState();
}

class _ListActivityScreenState extends State<ListActivityScreen> implements ListActivityScreenContract{
  ScrollController _scrollController = new ScrollController();
  User _modelUser;
  final GlobalKey _key = new GlobalKey();
  String _nextPageUrl="";
  ListActivityScreenPresenter _presenter;
  List<ActivityUser> _activityUser;

  bool _loadMore=false;
  _ListActivityScreenState(){
    _presenter = new ListActivityScreenPresenter(this);
  }
  @override
  void initState() {
    // TODO: implement initState
    _presenter.refreshUser();
    super.initState();
    this._scrollController.addListener((){
      if(this._scrollController.position.pixels==this._scrollController.position.maxScrollExtent){
        if(this._nextPageUrl!=null){
          this.getMoreData();
        }
      }
    });
  }
  void getMoreData(){
    setState(() {
      this._loadMore=true;
    });
    _presenter.refreshUser();
  }
  @override
  Widget build(BuildContext context) {
    if(this._activityUser==null){
      return SizedBox(
        key: this._key,
        height: 50,
        width: 50,
        child: CircularProgressIndicator(),
      );
    }
    else{
    return ListView.builder(
          shrinkWrap: true,
          key: this._key,
          controller: _scrollController,
          physics: ClampingScrollPhysics(),
          itemCount: this._activityUser.length,
          itemBuilder: (BuildContext context, int index){
            return this.singleActivity(context,this._activityUser[index]);
          },
        );
    }
  }
  Widget singleActivity(BuildContext context, ActivityUser activityUser){
    String username = activityUser.username;
    String text;
    if(activityUser.username.compareTo(this._modelUser.username)==0){
      username = "Anda";
    }
    if(activityUser.activityName.compareTo("post")==0){
      text = username + " "+activityUser.activityDesc+" "+ activityUser.activityData['places']['place_name']; 
    }
    else if(activityUser.activityName.compareTo("like")==0||activityUser.activityName.compareTo("comment")==0){
      text = username + " "+activityUser.activityDesc+" "+ activityUser.activityData['user']['username']; 
    }
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(right:10),
            width: 50,
            height: 50,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child:Icon(Icons.collections),
          ),
          Flexible(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Text(text),
                )
              ],
            ),
          ),
        ],
      ),
    ); 
  }

  @override
  void onLoadActivityError(FetchDataException error) {
    // TODO: implement onLoadActivityError
    print(error.toString());
  }

  @override
  void onLoadActivitySuccess(ActivityUserData activityUserData) {
    // TODO: implement onLoadActivitySuccess
    setState(() {
      if(activityUserData.nextPageUrl==null){
        this._nextPageUrl = null;
      }
      else{
        this._nextPageUrl = activityUserData.nextPageUrl;
      }
      if(this._loadMore){
        this._activityUser..addAll(activityUserData.activityUser.map((f)=>
          new ActivityUser.fromMap(f)
        ).toList());
        this._loadMore = false;
      }
      else{
        this._activityUser = activityUserData.activityUser.map((f)=>
          new ActivityUser.fromMap(f)
        ).toList();
      }
    });
  }

  @override
  void onRefreshUserError(FetchDataException error) {
    // TODO: implement onRefreshUserError
    print(error.toString());
  }

  @override
  Future onRefreshUserSuccess(User user) async {
    // TODO: implement onRefreshUserSuccess
    setState(() {
      this._modelUser = user;
    });
    print(this._modelUser.username);
    var db  = new DatabaseHelper();
    print(this._modelUser.jwtToken);
    bool updateToken = await db.updateToken(this._modelUser);
    if(updateToken){
      print("Loading Activity List");
      _presenter.loadActivity(this._modelUser.jwtToken, this._nextPageUrl);  
    }
    else{
      print("Update Token Error");
      Scaffold.of(context).showSnackBar(new SnackBar(content: Text("Something went wrong")));
    }
  }
}