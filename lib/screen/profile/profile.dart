import 'package:flutter/material.dart';
import 'package:martish/screen/activity/ListActivity.dart';
import 'package:martish/screen/post/ListUserPost.dart';
import 'package:martish/screen/profile/UserProfile.dart';
import 'package:martish/screen/profile/editProfile.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _showListPost = true;
  bool _showListActivity = false;
  int _currentScreenList = 0;
  final List<Widget> _bottomWidget = [
    ListUserPostScreen(),
    ListActivityScreen(),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: ListView(
        children: <Widget>[
          Column(
            children: <Widget>[
              UserProfileScreen(),
              SizedBox(height: 15.0),
              this._buttonPostOrActivity(),
              IndexedStack(
                index: this._currentScreenList,
                children: this._bottomWidget,
              ),
            ],
          ),
        ],
      ),
    );
  }
  Widget _buttonPostOrActivity(){
    return Container(
      padding: EdgeInsets.all(0),
      decoration: BoxDecoration(
        border: Border.all(color: Color(0xffe3e1e1)),
        color: Colors.white,
        ),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 5,
              child: Container(
                height: 60,
                child: new Material(
                  child:IconButton(
                    onPressed: (){
                      setState(() {
                        this._currentScreenList = 0;
                        this._showListPost = !this._showListPost;
                        this._showListActivity = !this._showListActivity;
                      });
                    },
                    icon: Icon(
                      Icons.collections,
                      color: this._showListPost ? Colors.blueAccent : Colors.black ,),
                    )
                ),
              ),
            ),
            Expanded(
              flex: 5,
              child: Container(
                height: 60,
                child: new Material(
                  child:IconButton(
                    onPressed: (){
                      setState(() {
                        this._currentScreenList=1;
                        this._showListPost = !this._showListPost;
                        this._showListActivity = !this._showListActivity;
                      });
                    },
                    icon: Icon(
                    Icons.list,
                    color: this._showListActivity? Colors.blueAccent : Colors.black),
                    )
                  ),
                ),
              )
          ],
      ),
    );
  }
}