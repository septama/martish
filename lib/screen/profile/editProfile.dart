import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:martish/data/profile/ProfileData.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

class EditProfileScreen extends StatefulWidget {
  final UserProfile userProfile;
  EditProfileScreen({@required this.userProfile});
  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
List<Asset> images = List<Asset>();  
Asset _profilePicture;
Asset _bannerPicture;
TextEditingController _username,
_realName,
_bio,
_website;
String _gender ="";
DateTime _birthday;


 String _error = 'No Error Dectected';
 @override
  void initState(){
    super.initState();
    this._username = new TextEditingController(text:this.widget.userProfile.username == null ? "" : this.widget.userProfile.username );
    this._realName = new TextEditingController(text:this.widget.userProfile.fullName  == null ? "" : this.widget.userProfile.fullName );
    this._bio = new TextEditingController(text:this.widget.userProfile.bio  == null ? "" : this.widget.userProfile.bio);
    this._website = new TextEditingController(text:this.widget.userProfile.website  == null ? "" : this.widget.userProfile.website);
    this.widget.userProfile.gender != "male" ||  this.widget.userProfile.gender != "female" ? this._gender = "" : this._gender = this.widget.userProfile.gender;
    this.widget.userProfile.birthday == null ? this._birthday = null : this._birthday = DateTime.parse(this.widget.userProfile.birthday);
  }
  Widget buildGridView(){
    return GridView.count(
      crossAxisCount: 1,
      shrinkWrap: true,
      children: List.generate(images.length, (index) {
        Asset asset = images[index];
        return AssetThumb(
          asset: asset,
          height: 150,
          width: 200,
        );
      }),
    );
  }
  Future <void> loadAssets()async{
    setState(() {
      images=List<Asset>();
    });
    List<Asset> resultList;
    String error;
    try {
      resultList = await MultiImagePicker.pickImages(maxImages: 1);
    } on PlatformException catch (e) {
      error = e.message;
    }
    if(!mounted){
      return;
    }
    setState(() {
      images = resultList;
      if (error == null) {_error = 'Sukses mengubah gambar';};
      Scaffold.of(context).showSnackBar(SnackBar(
        content:Text(_error),
      ));
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white70,
      appBar: new PreferredSize(
        preferredSize: Size.fromHeight(56),
        child: Builder(
          builder: (context)=> new AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              tooltip: "Kembali ke profil",
              onPressed: (){
                Navigator.pop(context);
              },
            ),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.send),
                onPressed: (){},
                tooltip: "Simpan Profil",
              ),
            ],
          ),
        ),
      ),
      body: ListView(
        children:<Widget>[
          Container(
            padding: EdgeInsets.all(10),
            color: Colors.white,
            child: Column(
              children: <Widget>[
                Container(
                  height:200 ,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(this.widget.userProfile.bannerPicture)
                    )
                  ),
                ),
                SizedBox(height:10),
                InkWell(
                  child: Text(
                    "Ubah foto banner",
                    style: TextStyle(color: Colors.blue,fontSize: 16),
                    ),
                    onTap: loadAssets,
                )
              ],
            ),
          ),
          Container(
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                   height: 150.0,
                   width: 150.0,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                        fit: BoxFit.cover,
                          image: NetworkImage(this.widget.userProfile.profilePicture),
                        ),
                        border: Border.all(
                          color: Colors.white,
                          width: 6.0
                        )
                    ),
                  ),
                SizedBox(height:10),
                InkWell(
                  child: Text(
                    "Ubah foto profile",
                    style: TextStyle(color: Colors.blue,fontSize: 16),
                    ),
                    onTap: loadAssets,
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            color:Colors.white,
            child:Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("Nama",style:TextStyle(fontSize: 16,color: Colors.grey)),
                TextField(
                  controller: this._realName,
                  maxLength: 32,
                  decoration: InputDecoration(
                    focusColor: Colors.blue,
                    hintText: "Nama",
                  ),
                )
              ],
            )
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            color:Colors.white,
            child:Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("Nama Pengguna",style:TextStyle(fontSize: 16,color: Colors.grey)),
                TextField(
                  controller: this._username,
                  maxLength: 32,
                  decoration: InputDecoration(
                    focusColor: Colors.blue,
                    hintText: "Nama Pengguna",
                  ),
                )
              ],
            )
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            color:Colors.white,
            child:Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("Website",style:TextStyle(fontSize: 16,color: Colors.grey)),
                TextField(
                  controller: this._website,
                  maxLength: 32,
                  decoration: InputDecoration(
                    focusColor: Colors.blue,
                    hintText: "Website",
                  ),
                )
              ],
            )
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            color:Colors.white,
            child:Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("Bio",style:TextStyle(fontSize: 16,color: Colors.grey)),
                TextField(
                  controller: this._realName,
                  maxLength: 32,
                  decoration: InputDecoration(
                    focusColor: Colors.blue,
                    hintText: "Bio",
                  ),
                )
              ],
            )
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            color:Colors.white,
            child:Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("Jenis Kelamin",style:TextStyle(fontSize: 16,color: Colors.grey)),
                DropdownButton(
                  items: [
                    DropdownMenuItem(
                      value: "",
                      child: Text("Pilih jenis kelamin"),
                    ),
                    DropdownMenuItem(
                      value: "male",
                      child: Text("Male"),
                    ),
                    DropdownMenuItem(
                      value: "female",
                      child: Text("Female"),
                    ),
                  ],
                  value:this._gender,
                  onChanged: (value){
                    setState(() {
                      this._gender = value;
                    });
                  },
                  isExpanded: true,
                ),
              ],
            )
          ),
          Container(
            color: Colors.white,
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("Tanggal lahir",style:TextStyle(fontSize: 16,color: Colors.grey)),
                DateTimePickerFormField(
                  inputType: InputType.date,
                  format: DateFormat("yyyy-MM-dd"),
                  initialDate: this._birthday == null ? DateTime(2019,1,1) : this._birthday,
                  editable: true,
                  decoration: InputDecoration(
                    hasFloatingPlaceholder: true,
                  ),
                  onChanged: (dt){
                    setState(() {
                      this._birthday = dt;
                    });
                  },
                ),
              ],
            ),
          ),
        ]
      ),
    );
  }
}