import 'package:flutter/material.dart';
import 'package:martish/data/database_helper.dart';
import 'package:martish/data/fetchDataException.dart';
import 'package:martish/data/post/PostData.dart';
import 'package:martish/data/profile/ProfileData.dart';
import 'package:martish/data/profile/ProfilePagePresenter.dart';
import 'package:martish/data/user/UserData.dart';
import 'package:martish/screen/profile/editProfile.dart';

class UserProfileScreen extends StatefulWidget {
  @override
  _UserProfileScreenState createState() => _UserProfileScreenState();
}

class _UserProfileScreenState extends State<UserProfileScreen> implements ProfileScreenContract{
  UserProfile _userProfile;
  User _modelUser;
  bool _loadScreen = true;
  final GlobalKey _key = new GlobalKey();
  ProfileScreenPresenter _presenter;
  _UserProfileScreenState(){
    _presenter = new ProfileScreenPresenter(this);
  }
  @override
  Widget build(BuildContext context) {
    if(this._loadScreen){
      return CircularProgressIndicator();
    }
    else{
      return Column(
        key: this._key,
        children: <Widget>[
          this.rowOne(),
          this.rowTwo(),
          SizedBox(height: 15.0),
          this.rowThree(),
          SizedBox(height: 15.0),
          this.rowFour(),
          SizedBox(height: 15.0),
          this._btnEditProfile(),
        ],
      );
    }
  }
   Widget _btnEditProfile(){
    return Container(
      height: 40.0,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Color(0xfff4f4f4)),
        ),
        color: Colors.white,
        onPressed: (){
          Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context)=> new EditProfileScreen(
              userProfile: this._userProfile,
            )
            ));
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Center(
                child: Text( "Edit Profile",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontFamily: 'Montserrat')),
                  )
                ],
              ),
          )
    );
  }
  Widget rowThree(){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          InkWell(
            onTap: (){

            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(this._userProfile.countPost.toString(),style: TextStyle(color:Colors.black,fontWeight: FontWeight.bold)),
                Text('post',style: TextStyle(color:Colors.black)),
                  ],
              ),
          ),
          InkWell(
            onTap: (){},
            child:Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(this._userProfile.countFollower.toString(),style: TextStyle(color:Colors.black,fontWeight: FontWeight.bold)),
                Text('follower',style: TextStyle(color:Colors.black)),
                ],
            ),
          ),
          InkWell(
            onTap: (){},
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(this._userProfile.countFollowing.toString(),style: TextStyle(color:Colors.black,fontWeight: FontWeight.bold)),
                Text('following',style: TextStyle(color:Colors.black)),
              ],
            ),
          )
          ],
        ),
      );
  }
  Widget rowFour(){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      alignment: Alignment.center,
      child: Text(this._userProfile.bio == null ? "" : this._userProfile.bio,style: TextStyle(fontSize: 18.0))
    );
  }
  Widget rowTwo(){
    return Container(
      alignment: Alignment.bottomCenter,
      height: 130.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(this._userProfile.fullName == null ? "" :  this._userProfile.fullName ,style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 28.0),
            ),
            SizedBox(width: 5.0),
            ],
        )
      );
  }
  Widget rowOne(){
    return Container(
      child: Stack(
        alignment: Alignment.bottomCenter,
        overflow: Overflow.visible,
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  height: 200.0,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(this._userProfile.bannerPicture)
                    )
                  ),
                ),
              )
            ],
          ),
          Positioned(
            top: 100.0,
            child: Container(
                   height: 190.0,
                   width: 190.0,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                        fit: BoxFit.cover,
                          image: NetworkImage(this._userProfile.profilePicture),
                        ),
                        border: Border.all(
                          color: Colors.white,
                          width: 6.0
                        )
                    ),
                  ),
            ),
          ],
        ),
    );
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _presenter.refreshUser();
  }
  @override
  void onLoadProfileError(FetchDataException error) {
    // TODO: implement onLoadProfileError
    print(error.toString());
  }

  @override
  void onLoadProfileSuccess(UserProfileData userProfileData) {
    // TODO: implement onLoadProfileSuccess
    if(userProfileData.success){
      print("Load profile success");
      setState(() {
          this._userProfile = userProfileData.userProfile;
          this._loadScreen=false;
        });
      print("Full Name"+this._userProfile.fullName);
    } else{
      setState(() {
        this._loadScreen = true;
      });
      Scaffold.of(context).showSnackBar(new SnackBar(content: Text("Something wrong with server")));
    }
  }

  @override
  void onRefreshUserError(FetchDataException error) {
    // TODO: implement onRefreshUserError
    print(error.toString());
  }

  @override
  void onRefreshUserSuccess(User user) async{
    // TODO: implement onRefreshUserSuccess
  setState(() {
      this._modelUser = user;
    });
    print(this._modelUser.username);
    var db  = new DatabaseHelper();
    print(this._modelUser.jwtToken);
    bool updateToken = await db.updateToken(this._modelUser);
    if(updateToken){
      print("Loading Feed");
      _presenter.loadProfile(this._modelUser.jwtToken);  
    }
    else{
      print("Update Token Error");
      Scaffold.of(context).showSnackBar(new SnackBar(content: Text("Something went wrong")));
    }
  }

  @override
  void onLoadPostError(FetchDataException error) {
    // TODO: implement onLoadPostError
  }

  @override
  void onLoadPostSuccess(DataPost dataPost) {
    // TODO: implement onLoadPostSuccess
  }
}