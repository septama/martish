import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:martish/component/profileComponent.dart';
import 'package:martish/data/activity/ActivityData.dart';
import 'package:martish/data/database_helper.dart';
import 'package:martish/data/fetchDataException.dart';
import 'package:martish/data/post/PostData.dart';
import 'package:martish/data/profile/ProfileData.dart';
import 'package:martish/data/profile/ProfilePagePresenter.dart';
import 'package:martish/data/user/UserData.dart';
import 'package:martish/screen/activity/ListActivity.dart';
class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}
class _ProfileScreenState extends State<ProfileScreen> implements ProfileScreenContract {
  var colorGray = Color(0xfff4f4f4);
  UserProfile _userProfile;
  bool _loadScreen = true;
  bool _refresh = false;
  User _modelUser;
  String _nextPageUrl="";
  List <Post> _listPost = new List();
  ProfileScreenPresenter _presenter;
  _ProfileScreenState(){
    _presenter = new ProfileScreenPresenter(this);
  }
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();
  GlobalKey <ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: colorGray,
      appBar: null,
      body: ListView(
        children: <Widget>[
          new Column(
            children: <Widget>[
              this.rowOne(),
              this.rowTwo(),
              SizedBox(height: 15.0),
              this.rowThree(),
              SizedBox(height: 15.0),
              this.rowFour(),
              SizedBox(height: 15.0),
              BtnEditProfile(),
              SizedBox(height: 15.0),
              BtnPostOrActivity(),
              ListActivityScreen(),
            ],
          )
        ],
      ),
    );
  }
  Widget rowThree(){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          InkWell(
            onTap: (){

            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(this._userProfile.countPost.toString(),style: TextStyle(color:Colors.black,fontWeight: FontWeight.bold)),
                Text('post',style: TextStyle(color:Colors.black)),
                  ],
              ),
          ),
          InkWell(
            onTap: (){},
            child:Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(this._userProfile.countFollower.toString(),style: TextStyle(color:Colors.black,fontWeight: FontWeight.bold)),
                Text('follower',style: TextStyle(color:Colors.black)),
                ],
            ),
          ),
          InkWell(
            onTap: (){},
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(this._userProfile.countFollowing.toString(),style: TextStyle(color:Colors.black,fontWeight: FontWeight.bold)),
                Text('following',style: TextStyle(color:Colors.black)),
              ],
            ),
          )
          ],
        ),
      );
  }
  Widget rowFour(){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      alignment: Alignment.center,
      child: Text(this._userProfile.bio,style: TextStyle(fontSize: 18.0))
    );
  }
  Widget rowTwo(){
    return Container(
      alignment: Alignment.bottomCenter,
      height: 130.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(this._userProfile.fullName,style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 28.0),
            ),
            SizedBox(width: 5.0),
            Icon(Icons.check_circle,color: Colors.blueAccent,)
            ],
        )
      );
  }
  Widget rowOne(){
    return Container(
      child: Stack(
        alignment: Alignment.bottomCenter,
        overflow: Overflow.visible,
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  height: 200.0,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(this._userProfile.bannerPicture)
                    )
                  ),
                ),
              )
            ],
          ),
          Positioned(
            top: 100.0,
            child: Container(
                   height: 190.0,
                   width: 190.0,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                        fit: BoxFit.cover,
                          image: NetworkImage(this._userProfile.profilePicture),
                        ),
                        border: Border.all(
                          color: Colors.white,
                          width: 6.0
                        )
                    ),
                  ),
            ),
          ],
        ),
    );
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _presenter.refreshUser();
  }
  @override
  void onLoadProfileError(FetchDataException error) {
    // TODO: implement onLoadProfileError
    print(error.toString());
  }

  @override
  void onLoadProfileSuccess(UserProfileData userProfileData) {
    // TODO: implement onLoadProfileSuccess
    if(userProfileData.success){
      print("Load profile success");
      setState(() {
          this._userProfile = userProfileData.userProfile;
          this._loadScreen=false;
        });
      print("Full Name"+this._userProfile.fullName);
    } else{
      setState(() {
        this._loadScreen = true;
      });
      _scaffoldKey.currentState.showSnackBar(new SnackBar(content:Text("Something wrong with server")));
    }
  }

  @override
  void onRefreshUserError(FetchDataException error) {
    // TODO: implement onRefreshUserError
    print(error.toString());
  }

  @override
  void onRefreshUserSuccess(User user) async{
    // TODO: implement onRefreshUserSuccess
  setState(() {
      this._modelUser = user;
    });
    print(this._modelUser.username);
    var db  = new DatabaseHelper();
    print(this._modelUser.jwtToken);
    bool updateToken = await db.updateToken(this._modelUser);
    if(updateToken){
      print("Loading Feed");
      _presenter.loadProfile(this._modelUser.jwtToken);  
    }
    else{
      print("Update Token Error");
      this._scaffoldKey.currentState.showSnackBar(new SnackBar(content: Text("Something went wrong"),));
    }
  }
  @override
  void onLoadPostError(FetchDataException error) {
    // TODO: implement onLoadPostError
    print(error.toString());
  }

  @override
  void onLoadPostSuccess(DataPost dataPost) {
    // TODO: implement onLoadPostSuccess
  }
}