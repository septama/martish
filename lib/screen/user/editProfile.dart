import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

class EditProfileScreen extends StatefulWidget {
  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
List<Asset> images = List<Asset>();  
 String _error = 'No Error Dectected';
 @override
  void initState(){
    super.initState();
  }
  Widget buildGridView(){
    return GridView.count(
      crossAxisCount: 1,
      shrinkWrap: true,
      children: List.generate(images.length, (index) {
        Asset asset = images[index];
        return AssetThumb(
          asset: asset,
          height: 150,
          width: 200,
        );
      }),
    );
  }
  Future <void> loadAssets()async{
    setState(() {
      images=List<Asset>();
    });
    List<Asset> resultList;
    String error;
    try {
      resultList = await MultiImagePicker.pickImages(maxImages: 1);
    } on PlatformException catch (e) {
      error = e.message;
    }
    if(!mounted){
      return;
    }
    setState(() {
      images = resultList;
      if (error == null) {_error = 'Sukses mengubah gambar';};
      Scaffold.of(context).showSnackBar(SnackBar(
        content:Text(_error),
      ));
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white70,
      appBar: new PreferredSize(
        preferredSize: Size.fromHeight(56),
        child: Builder(
          builder: (context)=> new AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              tooltip: "Kembali ke profil",
              onPressed: (){
                Navigator.pop(context);
              },
            ),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.send),
                onPressed: (){},
                tooltip: "Simpan Profil",
              ),
            ],
          ),
        ),
      ),
      body: ListView(
        children:<Widget>[
          Container(
            padding:EdgeInsets.all(15),
            color: Colors.white,
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Icon(Icons.image),
                    SizedBox(width: 5),
                    Text("Foto Sampul",style: TextStyle(fontWeight: FontWeight.bold)),
                    ],
                  ),
                  SizedBox(height: 15),
                  Stack(
                    alignment: Alignment.bottomCenter,
                    children: <Widget>[
                      Container(
                        child: buildGridView(),
                      ),
                      RaisedButton(
                        onPressed: loadAssets,
                        color: Colors.transparent,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(Icons.add_a_photo,color:Colors.white),
                            SizedBox(width: 15),
                            Text("Edit Banner",style: TextStyle(color:Colors.white,),)
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(height:5),
            Container(
              height: 250,
              padding:EdgeInsets.all(15),
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Icon(Icons.person_pin),
                      SizedBox(width: 5),
                      Text("Foto Profile",style: TextStyle(fontWeight: FontWeight.bold)),
                    ],
                  ),
                  Container(
                    height: 160,
                    width: 160,
                    child: Stack(
                      alignment: Alignment.center,
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(fit: BoxFit.cover, image: NetworkImage('http://cdn.ppcorn.com/us/wp-content/uploads/sites/14/2016/01/Mark-Zuckerberg-pop-art-ppcorn.jpg')),
                            shape: BoxShape.circle,
                          ),
                        ),
                        RaisedButton(
                          onPressed: (){},
                          color: Colors.transparent,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(Icons.add_a_photo,color:Colors.white),
                              SizedBox(width: 15),
                              Text("Edit Profil",style: TextStyle(color:Colors.white,),)
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height:5),
            Container(
              padding:EdgeInsets.all(15),
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Icon(Icons.chat_bubble_outline),
                      SizedBox(width: 5),
                      Text("Bio",style: TextStyle(fontWeight: FontWeight.bold)),
                    ],
                  ),
                  Container(
                    alignment: Alignment.center,
                    child: TextFormField(
                      maxLines: 3,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height:5),

        ]
      ),
    );
  }
}