import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
class FormPost extends StatefulWidget {
  @override
  _FormPostState createState() => _FormPostState();
}

class _FormPostState extends State<FormPost> {
 List<Asset> images = List<Asset>();  
 String _error = 'No Error Dectected';
  @override
  void initState(){
    super.initState();
  }
  Widget buildGridView(){
    return GridView.count(
      crossAxisCount: 3,
      shrinkWrap: true,
      children: List.generate(images.length, (index) {
        Asset asset = images[index];
        return AssetThumb(
          asset: asset,
          width: 100,
          height: 100,
        );
      }),
    );
  }
  Future <void> loadAssets()async{
    setState(() {
      images=List<Asset>();
    });
    List<Asset> resultList;
    String error;
    try {
      resultList = await MultiImagePicker.pickImages(maxImages: 3);
    } on PlatformException catch (e) {
      error = e.message;
    }
    if(!mounted){
      return;
    }
    setState(() {
      images = resultList;
      if (error == null) {_error = 'Pick Image Success';};
      Scaffold.of(context).showSnackBar(SnackBar(
        content:Text(_error),
      ));
    });
  }
  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
        Container(
          color:Colors.white,
          child: Column(
            children: <Widget>[
              SizedBox(height:10),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 15),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment:MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Container(
                            height:50,
                            width: 50,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                                  image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: NetworkImage('http://cdn.ppcorn.com/us/wp-content/uploads/sites/14/2016/01/Mark-Zuckerberg-pop-art-ppcorn.jpg'),
                                  ),
                            ),
                          )
                        ),
                        Expanded(
                          flex: 6,
                          child: Container(
                            padding: EdgeInsets.symmetric(horizontal: 10.0),
                            child: Text("Nanda Nur Septama",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16)),
                          )
                        )
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(height: 15),
              Container(
                padding:EdgeInsets.symmetric(horizontal: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Expanded(
                      flex: 2,
                      child:IconButton(
                        onPressed: (){},
                        icon: Icon(Icons.person_add,color:Colors.blueAccent),
                        tooltip: 'Tag Teman',
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: IconButton(
                        onPressed: loadAssets,
                        icon: Icon(Icons.add_a_photo),
                        tooltip: 'Tambah Gambar',
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child:IconButton(
                        onPressed: (){},
                        icon: Icon(Icons.add_location,color:Colors.redAccent),
                        tooltip: 'Pilih Lokasi',
                      ),
                    ),
                    Expanded(
                      flex: 6,
                      child: Container(),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 5),
              Container(
                padding:EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  maxLines: 5,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    hintText: "Post Sesuatu",
                    border: new OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey,width: 1,style: BorderStyle.solid)
                    ),
                  ),
                ),
              ),
              Container(
               child: buildGridView(),
              ),
            ],
          ),
        )
      ],
    );
  }
}