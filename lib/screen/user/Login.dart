import 'package:flutter/material.dart';
import 'package:martish/data/auth.dart';
import 'package:martish/data/database_helper.dart';
import 'package:martish/data/fetchDataException.dart';
import 'package:martish/data/user/UserData.dart';
import 'package:martish/data/user/LoginPresenter.dart';

class Login extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<Login> implements LoginContract, AuthStateListener{ 
  BuildContext _ctx;
  bool _isLoading=false;
  final formKey = new GlobalKey<FormState>();
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  LoginPresenter _presenter;
  String _username,_password;
  _LoginPageState(){
    _presenter = new LoginPresenter(this);
    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
  }  
  void _submit(){
    final form = formKey.currentState;
    if(form.validate()){
      setState(()=>_isLoading=true);
      form.save();
      _presenter.login(_username, _password); 
    }
  }
  @override
  onAuthStateChanged(AuthState state) {
    if(state == AuthState.LOGGED_IN)
      Navigator.of(_ctx).pushReplacementNamed("/dashboard");
  }
  @override
  Widget build(BuildContext context) {
    _ctx = context;
    var loginButton = new Container(
      child: 
      Container(
        child: Material(
          borderRadius: BorderRadius.circular(30.0),
          color: Colors.green,
          child: MaterialButton(
            minWidth: MediaQuery.of(context).size.width,
            padding: EdgeInsets.fromLTRB(20.0,15.0 , 20.0, 15.0),
            onPressed: _submit,
              child: Text("Masuk",
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),)
              ),
            ),
          )
        );

    var loginForm = new Container(
      padding: EdgeInsets.only(top: 25.0, left: 20.0, right: 20.0),
      child: Form(
        key : formKey,
        child: Column(
          children: <Widget>[
            new Container(
              child: Column(
                children: <Widget>[
                  TextFormField(
                    decoration: InputDecoration(
                      hintText: 'Username or Email',
                      labelStyle: TextStyle(
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.bold,
                        color: Colors.grey),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.green)
                        )
                      ),
                      onSaved: (val) =>_username = val,
                      validator : (val){
                        return val.length < 5
                        ? "Username must have atleast 5 chars"
                        : null;
                      },
                    ),
                    SizedBox(height: 10.0),
                    TextFormField(
                      onSaved: (val)=> _password=val,
                      validator : (val){
                        return val.length < 5
                        ? "password must have atleast 5 chars"
                        : null;
                      },
                      decoration: InputDecoration(
                        hintText: 'Password',
                        labelStyle: TextStyle(
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.bold,
                          color: Colors.grey),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.green)
                          )
                        ),
                        obscureText: true,
                    ),
                    SizedBox(height: 10.0),
                    Container(
                      alignment: Alignment(1.0, 0.0),
                      padding: EdgeInsets.only(top: 15.0, left: 20.0),
                      child: InkWell(
                        child: Text(
                        'Forgot Password',
                        style: TextStyle(
                          color: Colors.green,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Montserrat',
                          decoration: TextDecoration.underline
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 40.0), 
                    _isLoading ? new CircularProgressIndicator() : loginButton,
                    SizedBox(height: 20.0),
                    Container(
                      child:
                        Material(
                          borderRadius: BorderRadius.circular(30.0),
                          color: Colors.blue,
                          child: Container(
                            child:
                              MaterialButton(
                                padding: EdgeInsets.only(left : 60),
                                minWidth: MediaQuery.of(context).size.width,
                                onPressed: (){},
                                child: Row(
                                  children: <Widget>[
                                    Image(
                                      image: AssetImage('assets/images/facebook.png'),height: 22,width: 25
                                    ),
                                    Text('Masuk Dengan Facebook',
                                      style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white))  
                                  ],
                                ),
                              ),
                           )
                        )
                     ),
                    SizedBox(height: 15.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Belum Punya Akun ?',
                          style: TextStyle(fontFamily: 'Montserrat'),
                        ),
                        SizedBox(width: 5.0),
                        InkWell(
                          onTap: () {
                            Navigator.of(context).pushNamed('/signup');
                          },
                          child: Text(
                            'Daftar',
                            style: TextStyle(
                                color: Colors.green,
                                fontFamily: 'Montserrat',
                                fontWeight: FontWeight.bold,
                                decoration: TextDecoration.underline),
                          ),
                        )
                      ],
                    )     
                  ],
              ),
            )
          ],
        )
      )
    );
    return new Scaffold(
      resizeToAvoidBottomPadding: false,
      key:_scaffoldKey,
      body:
      ListView(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.only( bottom :100),
            alignment: Alignment.center,
            decoration: new BoxDecoration(
            gradient: new LinearGradient(
              begin: new Alignment(0.1, -0.9),
              end:  new Alignment(0.2, 0.7),
              colors:[
                const Color(0xFF37bf66),
                const Color(0xFFdfd0d0)
                  ],
                  stops:[0.1,1.0],  
                  )  
                ),
          child: 
            new Column(
                crossAxisAlignment: CrossAxisAlignment.start, 
                children: <Widget>[
                  Container(
                    child: Stack(
                      children: <Widget>[
                          Container(
                            child: Stack(
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.fromLTRB(90.0, 25.0, 0.0, 0.0),
                                  child: ImageIcon(AssetImage('assets/images/HailUpTextLogonoback2.png'),
                                  size: 170.0,
                                  color: Colors.white,),
                                  
                                    ),
                                  ],
                                ),
                              ),
        
                      ],
                    ),
                  ),
                 loginForm,      
                ],
          )),
        ],)     
    );
  }
  void showSnackBar(String text){
    _scaffoldKey.currentState.showSnackBar(new SnackBar( content:new Text(text)));
  }
  @override
  void onLoginError(FetchDataException error) {
    // TODO: implement onLoginError
    showSnackBar(error.toString());
    setState(() {
     _isLoading=false; 
    });

  }

  @override
  void onLoginSuccess(User user) async{
    // TODO: implement onLoginSuccess
    showSnackBar("Success Login");
    print("Login success");
    setState((){
     _isLoading=false;
    });
    var db = new DatabaseHelper();
    await db.saveUser(user); 
    var authStateProvider = new AuthStateProvider();
    authStateProvider.notify(AuthState.LOGGED_IN);
  }

}