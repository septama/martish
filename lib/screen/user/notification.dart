import 'dart:async';

import 'package:flutter/material.dart';
import 'package:martish/data/database_helper.dart';
import 'package:martish/data/fetchDataException.dart';
import 'package:martish/data/notification/NotificationData.dart';
import 'package:martish/data/notification/NotificationPresenter.dart';
import 'package:martish/data/user/UserData.dart';
import 'package:martish/screen/friend/profile.dart';

class NotificationScreen extends StatefulWidget {
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> implements ListNotificationContract{
  NotificationUser _notificationUser;
  ListNotificationPresenter _presenter;
  List<NotificationUser> _listNotification = new List();
  User _modelUser;
  bool isLoading = true;
  String _nextPageUrl="";
  final _scaffoldKey= new GlobalKey <ScaffoldState>();
  ScrollController _scrollController = new ScrollController();
  bool refresh = false;
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();
  _NotificationScreenState(){
    _presenter = new ListNotificationPresenter(this);
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key:this._scaffoldKey,
      appBar: AppBar(
        title:Text("Pemberitahuan"),
        automaticallyImplyLeading: false,
      ),
      body: listNotif(),
    );
  }

  Widget singleNotif(NotificationUser notificationUser){
    return Container(
    padding: EdgeInsets.symmetric(vertical: 10),
     child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(right:10),
          width: 50,
          height: 50,
          child:InkWell(
            onTap: (){
              Navigator.of(context).push(new MaterialPageRoute(
                builder: (BuildContext context)=>FriendProfile(
                  userId: notificationUser.userId,
                  username: notificationUser.username,
                ),
              ));
            },
            child:  notificationUser.profilePicture == null ? Icon(Icons.person) : CircleAvatar(child:Image.network(notificationUser.profilePicture)),
          ), 
        ),
        Flexible(
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                child: Text( notificationUser.username +" "+ notificationUser.desc),
              )
            ],
          ),
        ),
      ],
    ),
    );
  }
  Widget listNotif(){
    return RefreshIndicator(
      onRefresh: _onRefresh,
      key: _refreshIndicatorKey,
      child: ListView.builder(
        controller: _scrollController,
        physics: AlwaysScrollableScrollPhysics(),
        itemCount: this._listNotification.length,
        itemBuilder: (BuildContext context, int index){
          final NotificationUser notif = this._listNotification[index];
          return singleNotif(notif);
        },
      ),
    );
  }
  @override
  void initState(){
    // TODO: implement initState
    this.getMoreData();
    super.initState(); 
    this._scrollController.addListener((){
      if(this._scrollController.position.pixels==this._scrollController.position.maxScrollExtent){
        if(this._nextPageUrl!=null){
          this.getMoreData();
        }
      }
    });
  }
  void getMoreData()async{
    if(!isLoading){ 
      setState(() {
        isLoading=true;
      });
    }
    _presenter.refreshUser();
  }
  Future<Null> _onRefresh() {
    Completer<Null> completer = new Completer<Null>();

    new Timer(new Duration(seconds: 3), () {
      setState(() {
        this.refresh=true;
      });
      this.getMoreData();
      print("timer complete");
      completer.complete();
    });
    return completer.future;
  }
  @override
  void onGetNotificationFailed(FetchDataException error) {
    // TODO: implement onGetNotificationFailed
    print(error.toString());
  }

  @override
  void onGetNotificationSuccess(NotificationData notificationData) {
    // TODO: implement onGetNotificationSuccess
    if(notificationData.data.length!=0){
      if(this._listNotification.length==0){
        setState(() {
          this._listNotification = notificationData.data.map((f)=>NotificationUser.fromMap(f)).toList();
          this.isLoading =  false;
          this._nextPageUrl = notificationData.nextPageUrl != null ? notificationData.nextPageUrl : null;
        });
      }
      else{
        setState(() {
          this._listNotification..addAll(notificationData.data.map((f)=>NotificationUser.fromMap(f)).toList());
          this.isLoading =  false;
          this._nextPageUrl = notificationData.nextPageUrl != null ? notificationData.nextPageUrl : null;
        });
      }
    }
    else{
      this._scaffoldKey.currentState.showSnackBar(new SnackBar(content: Text("Nothing to show in your notification"),));
    }
  }

  @override
  void onRefreshUserError(FetchDataException error) {
    // TODO: implement onRefreshUserError
    print(error.toString());
  }

  @override
  void onRefreshUserSuccess(User user) async{
    // TODO: implement onRefreshUserSuccess
    setState(() {
      this._modelUser = user;
    });
    print(this._modelUser.username);
    var db  = new DatabaseHelper();
    print(this._modelUser.jwtToken);
    bool updateToken = await db.updateToken(this._modelUser);
    if(updateToken){
      print("Loading Notification");
      if(this.refresh){
        setState(() {
          this._listNotification = new List();
          this.refresh=false;
          this._nextPageUrl="";
        });
        _presenter.loadNotification(this._modelUser.jwtToken, this._nextPageUrl);
      }
      else{
       _presenter.loadNotification(this._modelUser.jwtToken, this._nextPageUrl);
      }
      
    }
    else{
      print("Update Token Error");
      this._scaffoldKey.currentState.showSnackBar(new SnackBar(content: Text("Something went wrong"),));
    }
  }

}


  
