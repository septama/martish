import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:martish/utils/auth_utils.dart';
import 'package:martish/utils/network_utils.dart';
class SignupPage extends StatefulWidget {
  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage>{
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  SharedPreferences _sharedPreferences;
  bool _isError = false;
  bool _obscureText = true;
  bool _isLoading = false;
  TextEditingController _emailController, _passwordController,_confirmPasswordController;
  String _errorText, _emailError, _passwordError,_confirmPasswordError;
  @override
  void initState() {
    super.initState();
    _fetchSessionAndNavigate();
    _emailController = new TextEditingController();
    _passwordController = new TextEditingController();
    _confirmPasswordController = new TextEditingController();
  }
  _fetchSessionAndNavigate()async{
    _sharedPreferences = await _prefs;
    String authToken = AuthUtils.getToken(_sharedPreferences);
    if(authToken!=null){
      Navigator.of(_scaffoldKey.currentContext).pushReplacementNamed('/dashboard');
    }
  }
  _registerUser()async{
    if(_valid()){
      var responseJson = await NetworkUtils.registerUser( _emailController.text, _passwordController.text
      );
      print(responseJson);
      if(responseJson==null){
        NetworkUtils.showSnackBar(_scaffoldKey, 'Something went wrong!');
      }
      else if(responseJson=='NetworkError'){
        NetworkUtils.showSnackBar(_scaffoldKey, null);
      }
      else if(responseJson['status']==false){
        NetworkUtils.showSnackBar(_scaffoldKey, responseJson['msg']);
      }
      else{
        AuthUtils.setEmail(_sharedPreferences, responseJson['data']['email']);
        Navigator.of(_scaffoldKey.currentContext)
          .pushReplacementNamed('/confirmRegister');
        }
      }
      else{
         setState(() {
          _isLoading = false;
          _emailError;
          _passwordError;
        });
      }
    }
    _valid(){
      bool valid = true;
      if(_emailController.text.isEmpty){
        valid=false;
        _emailError='Email cant be blank';
      }
      else if(!_emailController.text.contains("@")){
        valid = false;
        _emailError = "Invalid email";
      }
      if(_passwordController.text.isEmpty){
        valid = false;
        _passwordError = 'Password cant be blank';
      }
      if(_confirmPasswordController.text.isEmpty){
        valid = false;
        _passwordError = 'Confirm Password cant be blank';
      }
      else if(_confirmPasswordController.text != _passwordController.text){
        valid = false;
        _confirmPasswordError= 'Confirm password must same';
      }
      return valid;
    }
  @override
  Widget build(BuildContext context) {
    var registerButton = new Container(
      child: 
      Container(
        child: 
         Material(
           borderRadius: BorderRadius.circular(30.0),
           color: Colors.green,
           child: MaterialButton(
             minWidth: MediaQuery.of(context).size.width,
             padding: EdgeInsets.fromLTRB(20.0,15.0 , 20.0, 15.0),
             onPressed: _registerUser,
             child: Text("Daftar",
             textAlign: TextAlign.center,
             style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),)
            ),
          ),
        )
      );
    return new Scaffold(
        key:_scaffoldKey,
        resizeToAvoidBottomPadding: false,
        body: 
        ListView(
          children: <Widget>[ 
            Container(
              padding: const EdgeInsets.all(0.0),
              alignment: Alignment.center,
              decoration: new BoxDecoration(
                gradient: new LinearGradient(
                  begin: new Alignment(0.1, -0.9),
                  end:  new Alignment(0.2, 0.7),
                  colors:[
                    const Color(0xFF37bf66),
                    const Color(0xFFdfd0d0)
                      ],
                      stops:[0.1,1.0],  
                      )  
                    ),
              child: 
                new Column(
              crossAxisAlignment: CrossAxisAlignment.start, 
              children: <Widget>[
                Container(
                  child: Stack(
                    children: <Widget>[
                        Container(
                          child: Stack(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.fromLTRB(90.0, 25.0, 0.0, 0.0),
                                child: ImageIcon(AssetImage('assets/images/HailUpTextLogonoback2.png'),
                                size: 170.0,
                                color: Colors.white,),
                                
                                  ),
                                ],
                              ),
                            ),
      
                    ],
                  ),
                ),
                Container(
                    padding: EdgeInsets.only(top: 25.0, left: 20.0, right: 20.0),
                    child: Column(
                      children: <Widget>[
                        new Container(
                          child: new Form(
                            key: _formKey,
                            child: Column(
                              children: <Widget>[
                              SizedBox(height: 10.0),
                              TextFormField(
                                controller: _emailController,
                                decoration: InputDecoration(
                                    hintText: 'Email',
                                    errorText: _emailError,
                                    labelStyle: TextStyle(
                                        fontFamily: 'Montserrat',
                                        fontWeight: FontWeight.bold,
                                        color: Colors.grey),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(color: Colors.green))),
                                        
                              ),
                              SizedBox(height: 10.0),
                              TextFormField(
                                controller: _passwordController,
                                decoration: InputDecoration(
                                    hintText: 'Password',
                                    errorText: _passwordError,
                                    labelStyle: TextStyle(
                                        fontFamily: 'Montserrat',
                                        fontWeight: FontWeight.bold,
                                        color: Colors.grey),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(color: Colors.green))) ,                             
                                obscureText: true,
                              ),
                              SizedBox(height: 10.0),
                              TextFormField(
                                controller: _confirmPasswordController,
                                decoration: InputDecoration(
                                  errorText: _confirmPasswordError,
                                    hintText: 'Confirm Password',
                                    labelStyle: TextStyle(
                                        fontFamily: 'Montserrat',
                                        fontWeight: FontWeight.bold,
                                        color: Colors.grey),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(color: Colors.green))),
                          
                                obscureText: true,
                              ),
                              SizedBox(height: 50.0),
                              registerButton,
                            ],
                          ),
                        )
                      ),
                      SizedBox(height: 20.0),
                      Container(
                          height: 40.0,
                          color: Colors.transparent,
                          child: Container(
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: Colors.black,
                                    style: BorderStyle.solid,
                                    width: 1.0),
                                color: Colors.transparent,
                                borderRadius: BorderRadius.circular(20.0)),
                            child: InkWell(
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                              child: 
                              
                                  Center(
                                    child: Text('Kembali',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'Montserrat')),
                                  ),
                              
                              
                            ),
                          ),
                        ),
                      ],
                    )),
              ])
              
              ),
        ],
      )
    );
  }
}