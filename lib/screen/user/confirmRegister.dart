import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:martish/utils/auth_utils.dart';
import 'package:martish/utils/network_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
class ConfirmRegisterScreen extends StatefulWidget {
  @override
  _ConfirmRegisterScreenState createState() => _ConfirmRegisterScreenState();
}

class _ConfirmRegisterScreenState extends State<ConfirmRegisterScreen> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  var _email,_verifiedCode,_confirmResponse;
  TextEditingController _verifiedCodeController;
  String _verifiedCodeError;

  SharedPreferences _sharedPreferences;
  @override
  void initState() {
    super.initState();
    _fetchSessionAndNavigate();
    _verifiedCodeController = new TextEditingController();
  }
  _fetchSessionAndNavigate() async{
    _sharedPreferences = await _prefs;
    String email = _sharedPreferences.getString(AuthUtils.emailKey);
    print(email);
    setState(() {
      _email = email;
      _verifiedCode;
    });
  }
  _fetchConfirmRegister()async{
    if(_valid()){
      var responseJson = await NetworkUtils.confirmRegistration(_email, _verifiedCodeController.text);
    if(responseJson==null){
      NetworkUtils.showSnackBar(_scaffoldKey, 'Something went wrong!');
    }
    else if(responseJson=='NetworkError'){
      NetworkUtils.showSnackBar(_scaffoldKey, null);
    }
    else if(responseJson['success']==false){
      NetworkUtils.showSnackBar(_scaffoldKey, responseJson['msg']);
    }
    else{
      Navigator.of(_scaffoldKey.currentContext)
          .pushReplacementNamed('/login');
      }
    }
  }
  _valid(){
    bool valid=true;
    if(_verifiedCodeController.text.isEmpty){
      _verifiedCodeError = 'This field cant be blank';
      valid =false;
    }
    return valid;
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: null,
      body: Container(
          padding: const EdgeInsets.symmetric(vertical: 50,horizontal: 50),
          alignment: Alignment.center,
          decoration: new BoxDecoration(
            gradient: new LinearGradient(
            begin: new Alignment(0.1, -0.9),
            end:  new Alignment(0.2, 0.7),
            colors:[
              const Color(0xFF37bf66),
              const Color(0xFFdfd0d0)
            ],
              stops:[0.1,1.0],  
          )  
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text("Masukan Kode Verifikasi",style: TextStyle(color:Colors.white,fontWeight: FontWeight.bold,fontSize: 20)),
            SizedBox(height: 100),
            Container(
              width: 250,
              child:TextField(
                controller: _verifiedCodeController,
                maxLines: 1,
                maxLength: 6,
                keyboardType: TextInputType.number,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 48,
                  color: Colors.white,
                ),
                decoration: InputDecoration(
                  errorText: _verifiedCodeError,
                ),
              ),
            ),
            Container(
              child: RaisedButton(
                color: Colors.blue,
                onPressed: _fetchConfirmRegister,
                child: Container(
                  child: Text("Submit",style: TextStyle(color: Colors.white)),
                ),
              ),
            )
          ],
        )
      )
    );
  }
}