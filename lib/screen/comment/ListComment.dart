import 'dart:async';

import 'package:flutter/material.dart';
import 'package:martish/data/auth.dart';
import 'package:martish/data/comment/CommentData.dart';
import 'package:martish/data/comment/CommentPresenter.dart';
import 'package:martish/data/database_helper.dart';
import 'package:martish/data/fetchDataException.dart';
import 'package:martish/data/user/UserData.dart';
import 'package:martish/screen/friend/profile.dart';
class ListCommentScreen extends StatefulWidget {
  final int post_id;
  ListCommentScreen({@required this.post_id});
  @override
  _ListCommentScreenState createState() => _ListCommentScreenState();
}

class _ListCommentScreenState extends State<ListCommentScreen> implements CommentContract,AuthStateListener{
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();
  ScrollController _scrollController = new ScrollController();
  bool _refresh=false;
  String _nextPageUrl = "";
  int currentPage=1;
  int nextPage;
  var _commentTextController = new TextEditingController();
  String text="";
  User _userModel;
  CommentPresenter _presenter;
  List <Comment> _commentList = new List();
  _ListCommentScreenState(){
    _presenter = new CommentPresenter(this);
    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        automaticallyImplyLeading: true,
        title: Text("Komentar"),
      ),
      key: this._scaffoldKey,
      body: new Stack(
        children: <Widget>[
          new RefreshIndicator(
            key:_refreshIndicatorKey,
            onRefresh: _onRefresh,
            child: this.makeListCommentScreens(this._commentList),
          ),
          Positioned(
            bottom:0,
            left: 10,
            right: 10,
            child: Container(
              height: 70,
              padding: EdgeInsets.all(5),
              color: Colors.white,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Flexible(
                    child: TextFormField(
                      controller: _commentTextController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: "Berikan Komentar",
                      ),
                    ),
                  ),
                  FlatButton(
                    child: Text("Kirim"),
                    onPressed: _sumbitComment,
                  )
                ],
              )
            ),
          ),
        ],
      )
    );
  }
  @override
  void initState(){
    // TODO: implement initState
    this.getMoreData();
    super.initState(); 
    
    this._scrollController.addListener((){
      if(this._scrollController.position.pixels==this._scrollController.position.maxScrollExtent){
        if(this._nextPageUrl!=null){
          this.getMoreData();
        }
      }
    });
  }
  void _sumbitComment(){
    print(this._commentTextController.text);
    if(this._commentTextController.text.length==0){
      _scaffoldKey.currentState.showSnackBar(new SnackBar(content:Text("Isi kolom komentar terlebih dahulu")));
    }
    else{
      _presenter.sendComment(this._userModel.jwtToken, this.widget.post_id, this._commentTextController.text);
    }
  }
  void getMoreData()async{
    _presenter.refreshUser();
  }
  Widget makeListCommentScreens(List <Comment> values){
      return ListView.builder(
        controller: _scrollController,
        physics: const AlwaysScrollableScrollPhysics(),
        itemCount: values.length,
        itemBuilder : (BuildContext context, int index){
          return _comment(values[index]);
        },
      );
  }
  Future<Null> _onRefresh(){
    Completer<Null> completer = new Completer<Null>();

    new Timer(new Duration(seconds: 3), () {
      setState(() {
        this._refresh=true;
      });
      this.getMoreData();
      print("timer complete");
      completer.complete();
    });
    return completer.future;
  }
  Widget _comment(Comment comment){
    DateTime now = DateTime.now();
    int hoursAgo = (now.hour) - (DateTime.parse(comment.createdAt).hour -1);
    return new Container(
      padding: EdgeInsets.symmetric(horizontal: 15,vertical: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
            child: CircleAvatar(
              backgroundImage: NetworkImage(comment.profile_picture),
            ),
            onTap: (){
              Navigator.of(context).push(new MaterialPageRoute(
                builder:(BuildContext context)=>FriendProfile(
                  username: comment.username,
                  userId: comment.user_id,
                )
              ));
            },
          ),
          Flexible(
            child:Container(
            padding: EdgeInsets.symmetric(horizontal: 10,vertical: 3),
            child:RichText(
              text: TextSpan(
                text: comment.username.toString()+" ",
                style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(text: comment.content.text,style: TextStyle(fontWeight: FontWeight.normal)),
                ],
              ),
            ),
          )
          ),
        ],
      ),
    );
  }
  void loadComment()async{
    _presenter.refreshUser();
  }
  @override
  void onLoadCommentError(FetchDataException error) {
    // TODO: implement onLoadCommentError
    print(error.toString());
  }

  @override
  void onLoadCommentSuccess(List<Comment> commentList, int current_page, String url_next_page) {
    // TODO: implement onLoadCommentSuccess
    print("Success load comment");
    if(commentList.length !=0 ){
      if(this._commentList.length == 0){
        setState(() {
          this._commentList = commentList;
          this._nextPageUrl = url_next_page;
          this.currentPage = current_page + 1;
        });
        print(this._commentList);
        print(this._nextPageUrl);
      }
      else{
        setState(() {
          this._commentList..addAll(commentList);
          this._nextPageUrl = url_next_page;
          this.currentPage = current_page + 1;
        });
      }
    }
    else{
      this._scaffoldKey.currentState.showSnackBar(new SnackBar(content: Text("Tidak ada komentar pada postingan ini"),));
    }
  }

  @override
  void onRefreshUserError(FetchDataException error) {
    // TODO: implement onRefreshUserError
    print(error.toString());
  }

  @override
  Future onRefreshUserSuccess(User user) async {
    // TODO: implement onRefreshUserSuccess
    setState(() {
      this._userModel = user;
    });
    print(this._userModel.username);
    var db  = new DatabaseHelper();
    print(this._userModel.jwtToken);
    bool updateToken = await db.updateToken(this._userModel);
    if(updateToken){
      print("Loading Comment");
      if(this._refresh){
        setState(() {
          this._commentList = new List();
          this._refresh=false;
          this._nextPageUrl="";
        });
        if(this.nextPage!=null){
          String param = 
          "?post_id="+this.widget.post_id.toString()+
          "&page="+this.currentPage.toString();
          _presenter.loadComment(this._userModel.jwtToken,param);
        }
        else{
          String param = 
          "?post_id="+this.widget.post_id.toString();
          _presenter.loadComment(this._userModel.jwtToken,param);
        }
      }
      else{
        if(this.nextPage!=null){
          String param = 
          "?post_id="+this.widget.post_id.toString()+
          "&page="+this.currentPage.toString();
          _presenter.loadComment(this._userModel.jwtToken,param);
        }
        else{
          String param = 
          "?post_id="+this.widget.post_id.toString();
          _presenter.loadComment(this._userModel.jwtToken,param);
        }
      }
    }
    else{
      print("Update Token Error");
      this._scaffoldKey.currentState.showSnackBar(new SnackBar(content: Text("Something went wrong"),));
    }
  }

  @override
  void onAuthStateChanged(AuthState state) {
    // TODO: implement onAuthStateChanged
  }

  @override
  void onSendCommentError(FetchDataException error) {
    // TODO: implement onSendCommentError
    print(error.toString());
  }

  @override
  void onSendCommentSucces(String message, bool success,dynamic result) {
    // TODO: implement onSendCommentSucces
    if(success){
      _scaffoldKey.currentState.showSnackBar(new SnackBar(content: new Text(message)));
      setState(() {
        this._commentList.add(Comment.map(result));
      });
    }else{
      print(message);
      _scaffoldKey.currentState.showSnackBar(new SnackBar(content: new Text("Something went wrong")));
    }
  }
}