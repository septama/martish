import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:martish/data/database_helper.dart';
import 'package:martish/data/fetchDataException.dart';
import 'package:martish/data/post/FriendProfilePost.dart';
import 'package:martish/data/post/PostData.dart';
import 'package:martish/data/user/UserData.dart';
import 'package:martish/screen/post/singlePost.dart';

class ListFriendPost extends StatefulWidget {
  final int userId;
  ListFriendPost({@required this.userId});
  @override
  _ListFriendPostState createState() => _ListFriendPostState();
}

class _ListFriendPostState extends State<ListFriendPost> implements FriendProfilePostContract{
  ScrollController _scrollController = new ScrollController();
  User _modelUser;
  List <Post> _userPost;
  int temp=0;
  final GlobalKey _key = new GlobalKey();
  String _nextPageUrl="";
  bool _loadMore=false;
  FriendProfilePostPresenter _presenter;
  _ListFriendPostState(){
    _presenter = new FriendProfilePostPresenter(this);
  }
  @override
  Widget build(BuildContext context) {
    if(this._userPost==null){
      return SizedBox(
        key: this._key,
        height: 50,
        width: 50,
        child: CircularProgressIndicator(),
      );
    }
    else{
      return GridView.builder(
        controller: _scrollController,
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
        ),
        itemCount: this._userPost.length,
        key: _key,
        itemBuilder: (BuildContext context, int index){
          return _singlePost(context, this._userPost[index]);
        },
      );
    }
  }
  Widget _singlePost(BuildContext context, Post _userPost){
    return InkWell(
      child:Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(5),
        child: CachedNetworkImage(
              imageUrl: _userPost.content.imageUrl[0]['url'],
              placeholder: (context,url) => new Icon(Icons.refresh),
              errorWidget: (context,url,error) => new Icon(Icons.error),
        ),
      ),
      onTap: (){
        Navigator.of(context).push(
           MaterialPageRoute(
                  builder : (BuildContext context)=> new SinglePostScreen(
                    userPost: _userPost,
                  )
          ));
      },
    );
  }
  @override
  void initState() {
    // TODO: implement initState
    _presenter.refreshUser();
    super.initState();
    this._scrollController.addListener((){
      if(this._scrollController.position.pixels==this._scrollController.position.maxScrollExtent){
        if(this._nextPageUrl!=null){
          this.getMoreData();
        }
      }
    });
  }
  void getMoreData(){
    setState(() {
      this._loadMore=true;
    });
    _presenter.refreshUser();
  }
  @override
  void onLoadFriendPostSuccess(DataPost postData) {
    // TODO: implement onLoadFriendPostSuccess
    if(postData.getStatus){
      if(postData.getPost.length>0){
        if(this._loadMore){
          setState(() {
            this._userPost..addAll(postData.getPost.map((f)=> new Post.fromMap(f)).toList());
          });
        }
        else{
          setState(() {
            this._userPost = postData.getPost.map((f)=> new Post.fromMap(f)).toList();
          });
        }
      }
      if(postData.next_page==null){
        setState(() {
          this._nextPageUrl=null;
        });
      }
      else{
        int i = postData.getData['current_page'] + 1;
        setState(() {
          this._nextPageUrl = "&page=" + i.toString();
        });
      }
    }
    else{
      print(postData.getMessage);
      Scaffold.of(context).showSnackBar(new SnackBar(content: Text("Something went wrong")));
    }
  }

  @override
  void onRefreshUserError(FetchDataException error) {
    // TODO: implement onRefreshUserError
  }

  @override
  Future onRefreshUserSuccess(User user) async {
    // TODO: implement onRefreshUserSuccess
    setState(() {
      this._modelUser = user;
    });
    print(this._modelUser.username);
    var db  = new DatabaseHelper();
    print(this._modelUser.jwtToken);
    bool updateToken = await db.updateToken(this._modelUser);
    if(updateToken){
      print("Loading My Post");
      String param = "?user_id="+this.widget.userId.toString()+this._nextPageUrl;
      _presenter.loadFriendPost(this._modelUser.jwtToken, param);
    }
    else{
      print("Update Token Error");
      Scaffold.of(context).showSnackBar(new SnackBar(content: Text("Something went wrong")));
    }
  }

  @override
  void onLoadFriendPostError(FetchDataException error) {
    // TODO: implement onloadMyPostError
    print(error.toString());
  }
}