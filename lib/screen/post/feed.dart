import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:martish/data/auth.dart';
import 'package:martish/data/database_helper.dart';
import 'package:martish/data/fetchDataException.dart';
import 'package:martish/data/user/UserData.dart';
import 'package:martish/data/post/PostData.dart';
import 'package:martish/data/post/ListPostPresenter.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:martish/pivotScreen.dart';
import 'package:martish/screen/comment/ListComment.dart';
import 'package:martish/screen/friend/profile.dart';
import 'package:martish/screen/like/ListLiker.dart';
import 'package:martish/screen/user/formPost.dart';
import 'package:martish/screen/user/location.dart';
import 'package:martish/screen/user/notification.dart';
import 'package:martish/screen/profile/profile.dart';

class FeedScreen extends StatefulWidget {
  @override
  _FeedScreenState createState() => _FeedScreenState();
}

class _FeedScreenState extends State<FeedScreen> implements ListPostContract , AuthStateListener{
  
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  BuildContext _ctx;
  User _modelUser;
  Post _modelUserPost;
  List<Post> _listPost = new List();
  bool updateToken = false;
  ListPostPresenter _presenter;
  bool isLoading = true;
  bool refresh = false;
  String _nextPageUrl =  '';
  int post_like_id;
  final List<Widget> screens = [
    FeedScreen(),
    LocationScreen(),
    FormPost(),
    NotificationScreen(),
    ProfileScreen(),
  ];
  ScrollController _scrollController = new ScrollController();
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();

  _FeedScreenState(){
    _presenter = new ListPostPresenter(this);
    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
  }
  @override
  Widget build(BuildContext context) {
    this._ctx = context;
    return Scaffold(
      appBar: new AppBar(
        automaticallyImplyLeading: false,
        title: Text("Hailup"),
      ),
      key: _scaffoldKey,
      body: new RefreshIndicator(
        onRefresh: this._onRefresh,
        key: this._refreshIndicatorKey,
        child: _postWidget(),
      ),
    );
  }
  Widget _postWidget(){
    return Container(
        child: ListView.builder(
          controller: _scrollController,
          physics: const AlwaysScrollableScrollPhysics(),
          itemCount: this._listPost.length,
              itemBuilder: (BuildContext context, int index){
                final Post post = this._listPost[index];
                return _post(context,post);
          },
        ),
      );
  }
  Widget _post(BuildContext context, Post post){
    Icon likeIcon = post.is_like == 1 ? new Icon(FontAwesomeIcons.solidHeart,color:Colors.red) 
    : new Icon(FontAwesomeIcons.heart,color:Colors.black);
    return new Container(
      margin:EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
        Container(
          margin: EdgeInsets.all(5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                 children: <Widget>[
                   Container(
                     margin: EdgeInsets.only(right: 5),
                     child: InkWell(
                       child:
                       CircleAvatar(backgroundImage: NetworkImage(post.profile_picture)),
                       onTap :(){

                         print(this._modelUser.id);
                         if(this._modelUser.id == post.userId){
                           Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (BuildContext context)=>new PivotScreen(screens:this.screens,index:4)
                          ));
                         }
                         else{
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context)=>new FriendProfile(userId: post.userId,username: post.username,),
                          ));
                         }
                       }
                      )
                    ),
                    SizedBox(width: 5.0,),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(post.username == null ? "" : post.username,
                        style: TextStyle(fontFamily: 'Montserrat',fontWeight: FontWeight.bold,fontSize: 15.0),),
                        SizedBox(height: 5.0,),
                        Container(
                          alignment: Alignment.bottomLeft,
                          child:Text(post.place.place_name == null ? "" : post.place.place_name ,
                          style: TextStyle(fontSize: 10.0),) ,
                        ),
                      ],
                     ),
              ],
            ),
            IconButton(
              icon: Icon(Icons.more_vert),
              onPressed: (){},
            )
          ],
        ),
      ),
      Container(
        constraints: BoxConstraints(
        maxHeight: 300,
        ),
        child: SizedBox(
          height: 285.0,
          width: MediaQuery.of(context).size.width,
          child: Carousel(
            images:post.content.imageUrl.map((url){
              return CachedNetworkImage(
                imageUrl: url['url'].toString(),
                placeholder: (context,url) => new Icon(Icons.refresh),
                errorWidget: (context,url,error) => new Icon(Icons.error),
              );
            }).toList(),
            dotSize: 8.0,
            dotSpacing: 10.0,
            dotIncreasedColor: Colors.blueAccent,
            dotBgColor: Colors.transparent,
            autoplay: false,
            dotPosition: DotPosition.bottomCenter,
            dotColor: Colors.grey,
            indicatorBgPadding: 0,
            borderRadius: false,
            noRadiusForIndicator: true,
            moveIndicatorFromBottom: 25.0,
          ),
        ),
      ),
      Row(children: <Widget>[
                Row(
                  children: <Widget>[
                    Stack(
                      alignment: Alignment(0, 0),
                      children: <Widget>[
                            IconButton(
                              tooltip: "Like",
                              highlightColor: Colors.transparent,
                              focusColor: Colors.red,
                              icon: likeIcon,
                              onPressed: (){
                                setState(() {
                                  post.setIsLike();
                                  this.post_like_id = post.post_id;
                                  _presenter.refreshUser("send_like");
                                });
                              }
                            ),
                      ],
                    ),
                    Stack(
                      alignment: Alignment(0, 0),
                      children: <Widget>[
                        IconButton(
                          icon: Icon(FontAwesomeIcons.comment, color: Colors.black ,),
                          onPressed: (){},
                          ),
                      ],
                    ),
                    Stack(
                      alignment: Alignment(0,0),
                      children: <Widget>[
                        IconButton(
                          icon: Icon(FontAwesomeIcons.share, color: Colors.black,),
                          onPressed: (){},
                        )
                      ],)
                  ],
                ),
               
          ],
        ),
       Row(
         children: <Widget>[
            FlatButton(
              child: Text(post.count_like.toString()+ ' likes'),
              onPressed: (){
                Navigator.of(context).push(
                MaterialPageRoute(
                  builder : (BuildContext context)=> new ListLikerScreen(post_id: post.post_id)
                ));
              },
           ),
          ],
       ),
       Row(
         children: <Widget>[
          Flexible(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                 Container(
                  margin: EdgeInsets.symmetric(horizontal:10.0),
                  child:new Text(post.content.text,textAlign: TextAlign.justify,),
                ),
                
              ],
            )
          ),
          ],
       ),
       Row(children: <Widget>[
          post.count_comment != 0 ? FlatButton(
          child: Text("view all " + post.count_comment.toString() + " comments" ,),
            onPressed: (){
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder : (BuildContext context)=> new ListCommentScreen(post_id: post.post_id)
                ));
            },
          ) : null,     
        ],
      ),
    ],
  ),
  );
}
@override
  void initState(){
    // TODO: implement initState
    this.getMoreData();
    super.initState(); 
    this._scrollController.addListener((){
      if(this._scrollController.position.pixels==this._scrollController.position.maxScrollExtent){
        if(this._nextPageUrl!=null){
          this.getMoreData();
        }
      }
    });
  }
  void getMoreData()async{
    if(!isLoading){ 
      setState(() {
        isLoading=true;
      });
    }
    _presenter.refreshUser('load_feed');
  }
  Future<Null> _onRefresh() {
    Completer<Null> completer = new Completer<Null>();

    new Timer(new Duration(seconds: 3), () {
      setState(() {
        this.refresh=true;
      });
      this.getMoreData();
      print("timer complete");
      completer.complete();
    });
    return completer.future;
  }
  @override
  void onAuthStateChanged(AuthState state) {
    // TODO: implement onAuthStateChanged
  }

  @override
  void onLoadFeedError(FetchDataException error) {
    // TODO: implement onLoadFeedError
    print(error.toString());
  }

  @override
  void onLoadFeedSuccess(List<Post> listPost,String nextUrlPage) {
    // TODO: implement onLoadFeedSuccess
    if(listPost!=null){
      if(this._listPost.length==0){
        setState(() {
          this._listPost = listPost;
          this.isLoading =  false;
          this._nextPageUrl = nextUrlPage != null ? nextUrlPage : null;
        });
      }
      else{
        setState(() {
          print("Adding list ...");
          this._listPost..addAll(listPost);
          this._nextPageUrl = nextUrlPage != null ? nextUrlPage : null;
          this.isLoading =  false;
          print("Success add list");
        });
      }
    }
    else{
      this._scaffoldKey.currentState.showSnackBar(new SnackBar(content: Text("Nothing to show in your feed"),));
    }
  }

  @override
  void onRefreshUserError(FetchDataException error) {
    // TODO: implement onRefreshUserError
    print(error.toString());
  }

  @override
  void onRefreshUserSuccess(User user,String action) async{
    // TODO: implement onRefreshUserSuccess
    setState(() {
      this._modelUser = user;
    });
    print(this._modelUser.username);
    var db  = new DatabaseHelper();
    print(this._modelUser.jwtToken);
    bool updateToken = await db.updateToken(this._modelUser);
    if(updateToken){
      print("Loading Feed");
      print(action);
      if(this.refresh){
        setState(() {
          this._listPost = new List();
          this.refresh=false;
          this._nextPageUrl="";
        });
        if(action.compareTo("load_feed")==0){
            _presenter.loadFeed(this._modelUser.jwtToken, this._nextPageUrl);
          }
          else if(action.compareTo("send_like")==0){
            _presenter.sendLike(this._modelUser.jwtToken,this.post_like_id);
        }
      }
      else{
        if(action.compareTo("load_feed")==0){
            _presenter.loadFeed(this._modelUser.jwtToken, this._nextPageUrl);
          }
          else if(action.compareTo("send_like")==0){
            _presenter.sendLike(this._modelUser.jwtToken,this.post_like_id);
        }
      }
      
    }
    else{
      print("Update Token Error");
      this._scaffoldKey.currentState.showSnackBar(new SnackBar(content: Text("Something went wrong"),));
    }
  }

  @override
  void onSendingLikeError(FetchDataException error) {
    // TODO: implement onSendingLikeError
    print(error.toString());
  }

  @override
  void onSendingLikeSuccess(String message, bool status) {
    // TODO: implement onSendingLikeSuccess
    if(status){
      this._scaffoldKey.currentState.showSnackBar(new SnackBar(content: Text(message)));
    }
    else{
      print(message);
      this._scaffoldKey.currentState.showSnackBar(new SnackBar(content: Text("Something went wrong"),));
    }
  }
}