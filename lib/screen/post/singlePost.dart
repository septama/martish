import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:martish/data/database_helper.dart';
import 'package:martish/data/fetchDataException.dart';
import 'package:martish/data/post/PostData.dart';
import 'package:martish/data/post/SinglePostPresenter.dart';
import 'package:martish/data/user/UserData.dart';
import 'package:martish/screen/comment/ListComment.dart';
import 'package:martish/screen/like/ListLiker.dart';

class SinglePostScreen extends StatefulWidget {
  final Post userPost;
  SinglePostScreen({@required this.userPost});
  @override
  _SinglePostScreenState createState() => _SinglePostScreenState();
}

class _SinglePostScreenState extends State<SinglePostScreen> implements SinglePostScreenContract {
  int post_like_id;
  SinglePostScreenPresenter _presenter;

  User _modelUser;
  _SinglePostScreenState(){
    _presenter = new SinglePostScreenPresenter(this);
  }
  final GlobalKey <ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Kiriman"),
        automaticallyImplyLeading: true,
      ),
      key: _scaffoldKey,
      body: ListView(
        children: <Widget>[
          _post(context, this.widget.userPost),
        ],
      ),
    );
  }
  Widget _post(BuildContext context, Post post){
    Icon likeIcon = post.is_like == 1 ? new Icon(FontAwesomeIcons.solidHeart,color:Colors.red) 
    : new Icon(FontAwesomeIcons.heart,color:Colors.black);
    return new Container(
      margin:EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
        Container(
          margin: EdgeInsets.all(5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                 children: <Widget>[
                   Container(
                     margin: EdgeInsets.only(right: 5),
                     child: InkWell(
                       child:
                       CircleAvatar(backgroundImage: NetworkImage(post.profile_picture)),
                       onTap :(){

                       }
                      )
                    ),
                    SizedBox(width: 5.0,),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(post.username == null ? "" : post.username,
                        style: TextStyle(fontFamily: 'Montserrat',fontWeight: FontWeight.bold,fontSize: 15.0),),
                        SizedBox(height: 5.0,),
                        Container(
                          alignment: Alignment.bottomLeft,
                          child:Text(post.place.place_name == null ? " " : post.place.place_name ,
                          style: TextStyle(fontSize: 10.0),) ,
                        ),
                      ],
                     ),
              ],
            ),
            IconButton(
              icon: Icon(Icons.more_vert),
              onPressed: (){},
            )
          ],
        ),
      ),
      Container(
        constraints: BoxConstraints(
        maxHeight: 300,
        ),
        child: SizedBox(
          height: 285.0,
          width: MediaQuery.of(context).size.width,
          child: Carousel(
            images:post.content.imageUrl.map((url){
              return CachedNetworkImage(
                imageUrl: url['url'].toString(),
                placeholder: (context,url) => new Icon(Icons.refresh),
                errorWidget: (context,url,error) => new Icon(Icons.error),
              );
            }).toList(),
            dotSize: 8.0,
            dotSpacing: 10.0,
            dotIncreasedColor: Colors.blueAccent,
            dotBgColor: Colors.transparent,
            autoplay: false,
            dotPosition: DotPosition.bottomCenter,
            dotColor: Colors.grey,
            indicatorBgPadding: 0,
            borderRadius: false,
            noRadiusForIndicator: true,
            moveIndicatorFromBottom: 25.0,
          ),
        ),
      ),
      Row(children: <Widget>[
                Row(
                  children: <Widget>[
                    Stack(
                      alignment: Alignment(0, 0),
                      children: <Widget>[
                            IconButton(
                              tooltip: "Like",
                              highlightColor: Colors.transparent,
                              focusColor: Colors.red,
                              icon: likeIcon,
                              onPressed: (){
                                setState(() {
                                  post.setIsLike();
                                  this.post_like_id = post.post_id;
                                  _presenter.refreshUser("send_like");
                                });
                              }
                            ),
                      ],
                    ),
                    Stack(
                      alignment: Alignment(0, 0),
                      children: <Widget>[
                        IconButton(
                          icon: Icon(FontAwesomeIcons.comment, color: Colors.black ,),
                          onPressed: (){},
                          ),
                      ],
                    ),
                    Stack(
                      alignment: Alignment(0,0),
                      children: <Widget>[
                        IconButton(
                          icon: Icon(FontAwesomeIcons.share, color: Colors.black,),
                          onPressed: (){},
                        )
                      ],)
                  ],
                ),
               
          ],
        ),
       Row(
         children: <Widget>[
            FlatButton(
              child: Text(post.count_like.toString()+ ' likes'),
              onPressed: (){
                if(post.count_like !=0){
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder : (BuildContext context)=> new ListLikerScreen(post_id: post.post_id)
                    ));
                }
              },
           ),
          ],
       ),
       Row(
         children: <Widget>[
          Flexible(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                 Container(
                  margin: EdgeInsets.symmetric(horizontal:10.0),
                  child:new Text(post.content.text,textAlign: TextAlign.justify,),
                ),
                
              ],
            )
          ),
          ],
       ),
       Row(children: <Widget>[
          post.count_comment != 0 ? FlatButton(
          child: Text("view all " + post.count_comment.toString() + " comments" ,),
            onPressed: (){
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder : (BuildContext context)=> new ListCommentScreen(post_id: post.post_id)
                ));
            },
          ) : FlatButton(
              child: Text("Tidak ada komentar" ,),
                onPressed: (){
                  
                },
              ),     
        ],
      ),
    ],
  ),
  );
}

  @override
  void onRefreshUserError(FetchDataException error) {
    // TODO: implement onRefreshUserError
  }

  @override
  Future onRefreshUserSuccess(User user, String action) async {
    // TODO: implement onRefreshUserSuccess
    setState(() {
      this._modelUser = user;
    });
    print(this._modelUser.username);
    var db  = new DatabaseHelper();
    print(this._modelUser.jwtToken);
    bool updateToken = await db.updateToken(this._modelUser);
    if(updateToken){
     _presenter.sendLike(this._modelUser.jwtToken, this.widget.userPost.post_id);
    }
    else{
      print("Update Token Error");
      this._scaffoldKey.currentState.showSnackBar(new SnackBar(content: Text("Something went wrong"),));
    }
  }

  @override
  void onSendingLikeError(FetchDataException error) {
    // TODO: implement onSendingLikeError
  }

  @override
  void onSendingLikeSuccess(String message, bool status) {
    // TODO: implement onSendingLikeSuccess
    if(status){
      this._scaffoldKey.currentState.showSnackBar(new SnackBar(content: Text(message)));
    }
    else{
      print(message);
      this._scaffoldKey.currentState.showSnackBar(new SnackBar(content: Text("Something went wrong"),));
    }
  }
}