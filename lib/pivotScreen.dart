import 'package:flutter/material.dart';
class PivotScreen extends StatefulWidget{
  PivotScreen({this.screens,this.index});
  final List<Widget> screens;
  final int index;

  @override
  _PivotScreenState createState() => new _PivotScreenState();
}

class _PivotScreenState extends State<PivotScreen>{
  PageController _pageController;
  int _selectedScreen = 0;
  int _prevScreen=0;
  @override
  void initState() {
    super.initState();
    _pageController = PageController();
    this._selectedScreen = this.widget.index;
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Colors.white,
      appBar:_selectedScreen != 2 ? null : PreferredSize(
        preferredSize: Size.fromHeight(56),
        child: Builder(
          builder: (context) => new AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: (){
                setState(() {
                  _selectedScreen=_prevScreen;
                });
              },
            ),
            actions: <Widget>[
              IconButton(
                  icon: const Icon(Icons.send),
                  tooltip: 'Kirim',
                  onPressed: () {
                    Scaffold.of(context).showSnackBar(SnackBar(content: Text("Mengirim Postingan anda ..")));
                  },
                ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: new Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Colors.green,
          primaryColor: Colors.white,
          textTheme: Theme.of(context).textTheme.copyWith(
            caption: new TextStyle(color: Colors.white)
          ),
        ),
        child: BottomNavigationBar(
          backgroundColor: Colors.green,
          currentIndex: _selectedScreen,
          onTap: (int index){
          setState(() {
              _prevScreen = _selectedScreen;
              _selectedScreen = index;
          });
        },

        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.home,color: Colors.white),
            title: Text("Home",style: TextStyle(color: Colors.white))
          ),
          BottomNavigationBarItem(
            title: Text("Location",style: TextStyle(color: Colors.white)),
            icon: new Icon(Icons.place,color: Colors.white)
          ),
          BottomNavigationBarItem(
            title: Text("Buat Status",style: TextStyle(color: Colors.white)),
            icon: new Icon(Icons.add,color:Colors.white),
          ),
          BottomNavigationBarItem(
            title: Text("Notifikasi",style: TextStyle(color: Colors.white),),
            icon: Icon(Icons.notifications,color: Colors.white)
          ),
          BottomNavigationBarItem(
            icon:Icon(Icons.person,color: Colors.white),
            title: Text("Akun Saya",style: TextStyle(color: Colors.white))
          ),
        ],
      ),
      ),
      
      body: IndexedStack(
        index: _selectedScreen,
        children:widget.screens,
      ),
    );
  }
}