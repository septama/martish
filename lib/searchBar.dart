import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:martish/screen/user/ListPost.dart';
import 'package:martish/screen/user/profile.dart';
import 'package:martish/screen/user/location.dart';
import 'package:martish/screen/user/history.dart';

class SearchBar extends StatefulWidget {
  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> with SingleTickerProviderStateMixin {
  TabController controller;
  final pageOptions = [
    ListPostScreen(),
    HistoryScreen(),
    LocationScreen(),
    ProfileScreen()
  ];
  @override
  void initState(){
    super.initState();
    controller = new TabController(vsync: this,length: 4,initialIndex: 0);

  }
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: Colors.green[400]
    ));
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading:false,
        backgroundColor: Colors.green,
        title: Container(
          margin:EdgeInsets.symmetric(horizontal: 10, vertical: 8),
          decoration: BoxDecoration(
            color: Color.fromARGB(50, 255, 255, 255),
            borderRadius: BorderRadius.all(Radius.circular(20)),
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal:5.0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      border:InputBorder.none,
                      hintText: "Cari Sesuatu",
                      hintStyle: TextStyle(color:Colors.white),
                      icon: Icon(Icons.search, color: Colors.white),
                    ),
                  ),
                )
              ),
              Expanded(
                flex:0,
                child:Container(
                  padding: EdgeInsets.symmetric(horizontal: 5.0),
                  child: Row(
                    children: <Widget>[
                    IconButton(
                      onPressed: (){
                      },
                      icon: Icon(Icons.more_vert,color:Colors.white),
                    )
                  ],
                  ),
                )
              )
            ],
          ),
        ),
        bottom: TabBar(
          controller: controller,
          indicatorColor: Colors.white,
          labelColor: Colors.white,
          unselectedLabelColor: Colors.white,
          tabs: <Widget>[
            Tab(icon: Icon(Icons.home)),
            Tab(icon: Icon(Icons.receipt)),
            Tab(icon: Icon(Icons.shopping_cart)),
            Tab(icon: Icon(Icons.person))
          ]
        ),
      ),
      body: TabBarView(
        controller: controller,
        children:pageOptions
      ),
    );
  }
}