import 'package:martish/data/rest_data.dart';
import 'package:martish/model/user.dart';

abstract class LoginPageContract{
  void onLoginSuccess(User user);
  void onLoginError(String error);
}
class LoginPresenter{
  LoginPageContract _view;
  RestData api = new RestData();
  LoginPresenter(this._view);
  doLogin(String username,String password){
    api.login(username, password).then((User user){
      _view.onLoginSuccess(user);
    }).catchError((Exception error)=>_view.onLoginError(error.toString()));
  }
}
