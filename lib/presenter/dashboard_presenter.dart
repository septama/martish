import 'package:martish/data/database_helper.dart';
import 'package:martish/data/rest_data.dart';
import 'package:martish/model/user.dart';

abstract class DashboardPageContract{
  void onRefreshUserSuccess(User user);
  void onRefreshUserError(String error);
}
class DashboardPresenter{
  DashboardPageContract _view;
  RestData api = new RestData();
  DashboardPresenter(this._view);
  refreshUser(String token){
      api.refreshToken(token).then((User user){
        _view.onRefreshUserSuccess(user);
      }).catchError((Exception error){
        _view.onRefreshUserError(error.toString());
      });
  }
}
