import 'dart:async';
import 'package:flutter/material.dart';
import 'package:martish/screen/post/feed.dart';
import 'package:martish/screen/user/Login.dart';
import 'package:martish/screen/user/SignUp.dart';
import 'package:martish/screen/user/formPost.dart';
import 'package:martish/screen/user/notification.dart';
import 'pivotScreen.dart';
import 'package:martish/screen/user/history.dart';
import 'package:martish/screen/profile/profile.dart';
import 'package:martish/screen/user/location.dart';
import 'package:martish/screen/user/confirmRegister.dart';

void main() => runApp(MyApp());


class MyApp extends StatelessWidget {
  final List<Widget> screens = [
    FeedScreen(),
    LocationScreen(),
    FormPost(),
    NotificationScreen(),
    ProfileScreen(),
  ];

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'HailUp Login',
      debugShowCheckedModeBanner: false,
      routes: <String, WidgetBuilder>{
        
        '/login':(BuildContext context) =>  Login(),
        '/confirmRegister':(BuildContext context) => ConfirmRegisterScreen(),
        '/signup': (BuildContext context) => new SignupPage(),
        '/dashboard':(BuildContext context) => new PivotScreen(screens:this.screens,index:0),
        '/history':(BuildContext context) => new HistoryScreen(),
        '/profile': (BuildContext context) => new ProfileScreen(),
        '/location': (BuildContext context ) => new LocationScreen(),
      },
    
      theme: ThemeData(primarySwatch: Colors.green),
      home: SplashScreen(),
          );
        }
      
      }

 class SplashScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

      

class SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();
  loadData();
  }
  Future<Timer> loadData() async {
    return new Timer(Duration(seconds: 5), onDoneLoading );
  }
  onDoneLoading()async {
    Navigator.of(context).push(MaterialPageRoute(builder: (context)=> Login()));   
  }
  Widget build(BuildContext context) {
    return Scaffold(
      
        resizeToAvoidBottomPadding: false,
        body:
        Stack(fit: 
        StackFit.expand,
        children: <Widget>[
          Container(
          
          padding: const EdgeInsets.all(0.0),
          alignment: Alignment.center,
          decoration: new BoxDecoration(
            gradient: new LinearGradient(
              begin: new Alignment(0.1, -0.9),
              end:  new Alignment(0.2, 0.7),
              colors:[
                const Color(0xFF37bf66),
                const Color(0xFFdfd0d0)
                  ],
                  stops:[0.1,1.0],  
                  )  
                ),
        ),
        Container(  alignment: Alignment.center,
                    child: ImageIcon(AssetImage('assets/images/logohailup2white.png'),
                    size: 150.0,
                    color: Colors.white,),
        )
      ],)  
    ); 
  }
}
