import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
bool btnPostPressed = false;
bool btnActivityPressed=true;
class BtnPostOrActivity extends StatefulWidget {
  @override
  _BtnPostOrActivityState createState() => _BtnPostOrActivityState();
}
class BtnEditProfile extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return Container(
      height: 40.0,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Color(0xfff4f4f4)),
        ),
        color: Colors.white,
        onPressed: (){
          Navigator.pushNamed(context, '/editProfile');
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Center(
                child: Text( "Edit Profile",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontFamily: 'Montserrat')),
                  )
                ],
              ),
          )
    ); 
  }
}
class _BtnPostOrActivityState extends State<BtnPostOrActivity> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(0),
      decoration: BoxDecoration(
        border: Border.all(color: Color(0xffe3e1e1)),
        color: Colors.white,
        ),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 5,
              child: Container(
                height: 60,
                child: new Material(
                  child:IconButton(
                    onPressed: (){
                      setState(() {
                        btnActivityPressed = !btnActivityPressed;
                        btnPostPressed = !btnPostPressed;
                      });
                    },
                    icon: Icon(
                      Icons.collections,
                      color: btnPostPressed ? Colors.blueAccent : Colors.black ,),
                    )
                ),
              ),
            ),
            Expanded(
              flex: 5,
              child: Container(
                height: 60,
                child: new Material(
                  child:IconButton(
                    onPressed: (){
                      setState(() {
                        btnActivityPressed = !btnActivityPressed;
                        btnPostPressed = !btnPostPressed;
                      });
                    },
                    icon: Icon(
                    Icons.list,
                    color: btnActivityPressed ? Colors.blueAccent : Colors.black),
                    )
                  ),
                ),
              )
          ],
      ),
    );
  }
}
class BtnFollowFriend extends StatefulWidget{
  @override
  BtnFollowFriendState createState()=> BtnFollowFriendState();
}
class BtnFollowFriendState extends State<BtnFollowFriend>{
  bool pressedBtnFollow=false;
  @override
  Widget build(BuildContext context){
    return Container(
      height: 40.0,
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: RaisedButton(
        color:pressedBtnFollow ? Colors.white : Colors.blueAccent,
        onPressed: (){
          setState(() {
            pressedBtnFollow=!pressedBtnFollow;
            });
          },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Center(
                  child: Text(pressedBtnFollow ? "Unfollow" : "Follow",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: pressedBtnFollow ? Colors.black : Colors.white,
                    fontFamily: 'Montserrat')),
                     )
                    ],
                  ),
                )
    );
  }
}
class ProfileComponent extends StatelessWidget{
  static String _profilePicture;
  static String _bannerPicture;
  static String _fullName;
  static String _bio;
  ProfileComponent(profilePicture,bannerPicture,fullName,bio){
    _profilePicture = profilePicture;
    _fullName = fullName;
    _bannerPicture = bannerPicture;
    _bio = bio;
  }
  bool pressedBtnFollow=false;
  static Widget profilePicture =  Positioned(
                      top: 100.0,
                      child: Container(
                        height: 190.0,
                        width: 190.0,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: NetworkImage(_profilePicture),
                          ),
                          border: Border.all(
                            color: Colors.white,
                            width: 6.0
                          )
                        ),
                      ),
                    );
static Widget bannerPicture = 
  Row(
    children: <Widget>[
      Expanded(
        child: Container(
          height: 200.0,
          decoration: BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.cover,
              image: NetworkImage(_bannerPicture)
            )
          ),
        ),
      )
    ],
  );         
  var profileAndBannerPicture = 
  Container(
    child: Stack(
      alignment: Alignment.bottomCenter,
      overflow: Overflow.visible,
      children: <Widget>[
        bannerPicture,
        profilePicture
        ],
      ),
    );
var realName = Container(
  alignment: Alignment.bottomCenter,
  height: 130.0,
  child: Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      Text('Nanda Nur Septama',style: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 28.0),
        ),
        SizedBox(width: 5.0),
        Icon(Icons.check_circle,color: Colors.blueAccent,)
        ],
    )
  );
  var userBio = 
  Container(
    padding: EdgeInsets.symmetric(horizontal: 10),
    alignment: Alignment.center,
    child: Text(_bio,style: TextStyle(fontSize: 18.0))
  );
  var userPostAndFollow = 
  Container(
    padding: EdgeInsets.symmetric(horizontal: 10),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text('192',style: TextStyle(color:Colors.black,fontWeight: FontWeight.bold)),
            Text('post',style: TextStyle(color:Colors.black)),
              ],
            ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text('192',style: TextStyle(color:Colors.black,fontWeight: FontWeight.bold)),
              Text('follower',style: TextStyle(color:Colors.black)),
              ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text('192',style: TextStyle(color:Colors.black,fontWeight: FontWeight.bold)),
                  Text('following',style: TextStyle(color:Colors.black)),
                ],
              ),
            ],
          ),
      );
  var singleActivity = 
  Container(
    padding: EdgeInsets.all(0),
    child: Container(
      padding:EdgeInsets.symmetric(vertical:15,horizontal: 10),
      color: Colors.white,
      child: InkWell(
        child: Container(
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    color:Colors.blueAccent,
                    shape: BoxShape.circle
                  ),
                  child: Icon(
                    Icons.hotel,color: Colors.white,
                  ),
                ),
              ),
              Expanded(
                flex: 5,
                child: RichText(
                  text: TextSpan(
                    children: <TextSpan>[
                      TextSpan(text: "Anda",style: TextStyle(fontWeight: FontWeight.bold,color:Colors.black)),
                      TextSpan(text:" mengunjungi",style: TextStyle(color: Colors.black)),
                      TextSpan(text: " Hotel Santika",style: TextStyle(fontWeight: FontWeight.bold,color:Colors.black)),
                      TextSpan(text:" 2 hari yang lalu.",style: TextStyle(color: Colors.black)),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex:1,
                child:Icon(Icons.more_vert),
              ),
            ],
          ),
        ),
      )
    )
  ); 
  Widget getListActivity(){
    var listView = ListView(
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
      children: <Widget>[
        this.singleActivity,
        this.singleActivity,
        this.singleActivity,
        this.singleActivity,
      ],
    );
    return listView;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return null;
  }
}
