import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'auth_utils.dart';

class NetworkUtils{
  static final String host = developmentHost;
  static final String developmentHost = 'http://192.168.43.85/alice/public';
  static final String productionHost = 'https://api.hailup.com';

  static dynamic loginUser(String username, String password)async{
    var uri = host + AuthUtils.login_url;
    try{
      final response = await http.post(uri,body:{
        'username':username,
        'password':password
      });
      final responseJson = json.decode(response.body);
      return responseJson;
    }
    catch(exception){
      print(exception);
      if(exception.toString().contains('SocketException')) {
        return 'NetworkError';
      } else {
        return null;
      }
    }
  }
  static dynamic registerUser(String email, String password)async{
    var uri = host + AuthUtils.register_url;
    print(email);
    print(password);
    print(uri);
    try{
      final response = await http.post(uri,body:{
        'password':password,
        'email':email,
        'confirm_password':password
      });
      final responseJson = json.decode(response.body);
      return responseJson;
    }
    catch(exception){
      print(exception);
      if(exception.toString().contains('SocketException')) {
        return 'NetworkError';
      } else {
        return null;
      }
    }
  }
  static dynamic confirmRegistration(String email, String verified_code)async{
    var uri = host + AuthUtils.confirm_register;
    try{
      final response = await http.post(uri,body:{
        'verified_code':verified_code,
        'email':email
      });
      final responseJson = json.decode(response.body);
      return responseJson;
    }
    catch(exception){
      print(exception);
      if(exception.toString().contains('SocketException')) {
        return 'NetworkError';
      } else {
        return null;
      }
    }
  }
  static showSnackBar(GlobalKey<ScaffoldState> scaffoldKey, String message) {
    scaffoldKey.currentState.showSnackBar(
      new SnackBar(
        content: new Text(message ?? 'You are offline'),
      )
    );
  }
  static fetch (var authToken, var endPoint)async{
    var uri = Uri.parse(host+endPoint);
    try{
      final response = await http.get(
        uri,
        headers:{HttpHeaders.authorizationHeader: 'Bearer ' + authToken}
      );
      final responseJson = json.decode(response.body);
      return responseJson;
    } catch (exception){
      print(exception);
      if(exception.toString().contains('SocketException')) {
        return 'NetworkError';
      } else {
        return null;
      }
    }
  }
  static fetchUserFeed (var authToken, var endPoint)async{
    var uri = Uri.parse(host+endPoint);
    uri = uri.replace(queryParameters: <String,String>{'token':authToken});
    try{
      final response = await http.get(
        uri,
      );
      final responseJson = json.decode(response.body);
      return responseJson;
    } catch (exception){
      print(exception);
      if(exception.toString().contains('SocketException')) {
        return 'NetworkError';
      } else {
        return null;
      }
    }
  }
}