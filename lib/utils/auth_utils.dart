import 'package:shared_preferences/shared_preferences.dart';

class AuthUtils{
  static final String prefixUrl = '/api/v1';
  static final String register_url = prefixUrl+'/register';
  static final String login_url = prefixUrl+'/login';
  static final String confirm_register = prefixUrl+'/confirmation';
  static final String userFeed = prefixUrl+'/user/feed';
  static final String refreshToken = prefixUrl+'/refresh';

  static final String authTokenKey = 'auth_token';
  static final String userIdKey = 'user_id';
  static final String usernameKey = 'username';
  static final String emailKey = 'email';
  static String getToken(SharedPreferences prefs){
    return prefs.getString(authTokenKey);
  }
  static setToken(SharedPreferences prefs,String token){
    prefs.setString(authTokenKey, token);
  }
  static setEmail(SharedPreferences prefs, String email){
    prefs.setString(emailKey, email);
  }
  static String getEmail(SharedPreferences prefs){
    return prefs.getString(emailKey);
  }
  static insertDetails(SharedPreferences prefs, var response) {
    prefs.setString(authTokenKey, response['token']);
    var user = response['user'];
    prefs.setInt(userIdKey, user['id']);
    prefs.setString(usernameKey, user['username']);
    prefs.setString(emailKey, user['email']);
  }
}